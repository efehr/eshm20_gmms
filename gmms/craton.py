"""
Implements the ESHM20 GMM for application to the cratonic region of Europe
as described in Weatherill & Cotton (2020)

"""
from copy import copy
from typing import Union, Dict, List, Optional, Tuple
import numpy as np
import pandas as pd
from scipy.stats import chi2
from gmms.base import coefficients_to_dictionary, get_coefficients_for_imt, GMM


# Coefficients for the linear model, taken from the electronic supplement
# to Stewart et al., (2017)
COEFFS_LINEAR = coefficients_to_dictionary("""\
    imt           c      v1       v2      vf  sigma_vc  sigma_L  sigma_U
    pgv      -0.449   331.0    760.0   314.0     0.251    0.306    0.334
    pga      -0.290   319.0    760.0   345.0     0.300    0.345    0.480
    0.010    -0.290   319.0    760.0   345.0     0.300    0.345    0.480
    0.020    -0.303   319.0    760.0   343.0     0.290    0.336    0.479
    0.030    -0.315   319.0    810.0   342.0     0.282    0.327    0.478
    0.050    -0.344   319.0   1010.0   338.0     0.271    0.308    0.476
    0.075    -0.348   319.0   1380.0   334.0     0.269    0.285    0.473
    0.100    -0.372   317.0   1900.0   319.0     0.270    0.263    0.470
    0.150    -0.385   302.0   1500.0   317.0     0.261    0.284    0.402
    0.200    -0.403   279.0   1073.0   314.0     0.251    0.306    0.334
    0.250    -0.417   250.0    945.0   282.0     0.238    0.291    0.357
    0.300    -0.426   225.0    867.0   250.0     0.225    0.276    0.381
    0.400    -0.452   217.0    843.0   250.0     0.225    0.275    0.381
    0.500    -0.480   217.0    822.0   280.0     0.225    0.311    0.323
    0.750    -0.510   227.0    814.0   280.0     0.225    0.330    0.310
    1.000    -0.557   255.0    790.0   300.0     0.225    0.377    0.361
    1.500    -0.574   276.0    805.0   300.0     0.242    0.405    0.375
    2.000    -0.584   296.0    810.0   300.0     0.259    0.413    0.388
    3.000    -0.588   312.0    820.0   313.0     0.306    0.410    0.551
    4.000    -0.579   321.0    821.0   322.0     0.340    0.405    0.585
    5.000    -0.558   324.0    825.0   325.0     0.340    0.409    0.587
    7.500    -0.544   325.0    820.0   328.0     0.345    0.420    0.594
    10.00    -0.507   325.0    820.0   330.0     0.350    0.440    0.600
    """)

# Coefficients for the nonlinear model, taken from Table 2.1 of
# Hashash et al., (2017)
COEFFS_NONLINEAR = coefficients_to_dictionary("""\
    imt          f3         f4         f5     Vc   sigma_c
    pgv     0.06089   -0.08344   -0.00667   2257.0   0.120
    pga     0.07520   -0.43755   -0.00131   2990.0   0.120
    0.010   0.07520   -0.43755   -0.00131   2990.0   0.120
    0.020   0.05660   -0.41511   -0.00098   2990.0   0.120
    0.030   0.10360   -0.49871   -0.00127   2990.0   0.120
    0.050   0.16781   -0.58073   -0.00187   2990.0   0.120
    0.075   0.17386   -0.53646   -0.00259   2990.0   0.120
    0.100   0.15083   -0.44661   -0.00335   2990.0   0.120
    0.150   0.14272   -0.38264   -0.00410   2335.0   0.120
    0.200   0.12815   -0.30481   -0.00488   1533.0   0.120
    0.250   0.13286   -0.27506   -0.00564   1318.0   0.135
    0.300   0.13070   -0.22825   -0.00655   1152.0   0.150
    0.400   0.09414   -0.11591   -0.00872   1018.0   0.150
    0.500   0.09888   -0.07793   -0.01028    939.0   0.150
    0.750   0.06101   -0.01780   -0.01456    835.0   0.125
    1.000   0.04367   -0.00478   -0.01823    951.0   0.060
    1.500   0.00480   -0.00086   -0.02000    882.0   0.050
    2.000   0.00164   -0.00236   -0.01296    879.0   0.040
    3.000   0.00746   -0.00626   -0.01043    894.0   0.040
    4.000   0.00269   -0.00331   -0.01215    875.0   0.030
    5.000   0.00242   -0.00256   -0.01325    856.0   0.020
    7.500   0.04219   -0.00536   -0.01418    832.0   0.020
    10.00   0.05329   -0.00631   -0.01403    837.0   0.020
    """)


# Note that the coefficient values at 0.1 s have been smoothed with respect
# to those needed in order to reproduce Figure 5 of Petersen et al. (2019)
# The original f760i was 0.674 +/- 0.366, and the values below are taken
# from the US NSHMP software
COEFFS_F760 = coefficients_to_dictionary("""\
    imt       f760i     f760g   f760is   f760gs
    pgv      0.3753     0.297    0.313    0.117
    pga      0.1850     0.121    0.434    0.248
    0.010    0.1850     0.121    0.434    0.248
    0.020    0.1850     0.031    0.434    0.270
    0.030    0.2240     0.000    0.404    0.229
    0.050    0.3370     0.062    0.363    0.093
    0.075    0.4750     0.211    0.322    0.102
    0.100    0.5210     0.338    0.293    0.088
    0.150    0.5860     0.470    0.253    0.066
    0.200    0.4190     0.509    0.214    0.053
    0.250    0.3320     0.509    0.177    0.052
    0.300    0.2700     0.498    0.131    0.055
    0.400    0.2090     0.473    0.112    0.060
    0.500    0.1750     0.447    0.105    0.067
    0.750    0.1270     0.386    0.138    0.077
    1.000    0.0950     0.344    0.124    0.078
    1.500    0.0830     0.289    0.112    0.081
    2.000    0.0790     0.258    0.118    0.088
    3.000    0.0730     0.233    0.111    0.100
    4.000    0.0660     0.224    0.120    0.109
    5.000    0.0640     0.220    0.108    0.115
    7.500    0.0560     0.216    0.082    0.130
    10.00    0.0530     0.218    0.069    0.137
    """)


COEFFS_USGS_SIGMA_PANEL = coefficients_to_dictionary("""\
    imt         t1       t2       t3       t4     ss_a     ss_b    s2s1    s2s2
    pgv     0.3633   0.3532   0.3340   0.3136   0.4985   0.3548   0.487   0.458
    pga     0.4436   0.4169   0.3736   0.3415   0.5423   0.3439   0.533   0.566
    0.010   0.4436   0.4169   0.3736   0.3415   0.5423   0.3439   0.533   0.566
    0.020   0.4436   0.4169   0.3736   0.3415   0.5410   0.3438   0.537   0.577
    0.030   0.4436   0.4169   0.3736   0.3415   0.5397   0.3437   0.542   0.598
    0.050   0.4436   0.4169   0.3736   0.3415   0.5371   0.3435   0.583   0.653
    0.075   0.4436   0.4169   0.3736   0.3415   0.5339   0.3433   0.619   0.633
    0.100   0.4436   0.4169   0.3736   0.3415   0.5308   0.3431   0.623   0.590
    0.150   0.4436   0.4169   0.3736   0.3415   0.5247   0.3466   0.603   0.532
    0.200   0.4436   0.4169   0.3736   0.3415   0.5189   0.3585   0.578   0.461
    0.250   0.4436   0.4169   0.3736   0.3415   0.5132   0.3694   0.554   0.396
    0.300   0.4436   0.4169   0.3736   0.3415   0.5077   0.3808   0.527   0.373
    0.400   0.4436   0.4169   0.3736   0.3415   0.4973   0.4004   0.491   0.339
    0.500   0.4436   0.4169   0.3736   0.3415   0.4875   0.4109   0.472   0.305
    0.750   0.4436   0.4169   0.3736   0.3415   0.4658   0.4218   0.432   0.273
    1.000   0.4436   0.4169   0.3736   0.3415   0.4475   0.4201   0.431   0.257
    1.500   0.4436   0.4169   0.3736   0.3415   0.4188   0.4097   0.424   0.247
    2.000   0.4436   0.4169   0.3736   0.3415   0.3984   0.3986   0.423   0.239
    3.000   0.4436   0.4169   0.3736   0.3415   0.3733   0.3734   0.418   0.230
    4.000   0.4436   0.4169   0.3736   0.3415   0.3604   0.3604   0.412   0.221
    5.000   0.4436   0.4169   0.3736   0.3415   0.3538   0.3537   0.404   0.214
    7.500   0.4436   0.4169   0.3736   0.3415   0.3482   0.3481   0.378   0.201
    10.00   0.4436   0.4169   0.3736   0.3415   0.3472   0.3471   0.319   0.193
    """)


# ==========================================================================
# Functions related to the NGA East Site Amplification Models
# Stewart et al. (2020); Hashash et al., (2020)
# ==========================================================================

# Seven constants: vref, vL, vU, vw1, vw2, wt1 and wt2
NGA_EAST_CONSTANTS = {"vref": 760., "vL": 200., "vU": 2000.0,
                      "vw1": 600.0, "vw2": 400.0, "wt1": 0.767, "wt2": 0.1}


def _get_f760(C_F760: Dict, vs30: np.ndarray, is_stddev: bool = False) -> np.ndarray:
    """
    Returns very hard rock to hard rock (Vs30 760 m/s) adjustment factor
    taken as the Vs30-dependent weighted mean of two reference condition
    factors: for impedence and for gradient conditions. The weighting
    model is described by equations 5 - 7 of Stewart et al. (2019)
    """
    wimp = (NGA_EAST_CONSTANTS["wt1"] - NGA_EAST_CONSTANTS["wt2"]) *\
        (np.log(vs30 / NGA_EAST_CONSTANTS["vw2"]) /
         np.log(NGA_EAST_CONSTANTS["vw1"] / NGA_EAST_CONSTANTS["vw2"])) +\
        NGA_EAST_CONSTANTS["wt2"]
    wimp[vs30 >= NGA_EAST_CONSTANTS["vw1"]] = NGA_EAST_CONSTANTS["wt1"]
    wimp[vs30 < NGA_EAST_CONSTANTS["vw2"]] = NGA_EAST_CONSTANTS["wt2"]
    wgr = 1.0 - wimp
    if is_stddev:
        return wimp * C_F760["f760is"] + wgr * C_F760["f760gs"]
    else:
        return wimp * C_F760["f760i"] + wgr * C_F760["f760g"]


def _get_fv(C_LIN: Dict, vs30: np.ndarray, f760: np.ndarray) -> np.ndarray:
    """
    Returns the Vs30-dependent component of the mean linear amplification
    model, as defined in equation 3 of Stewart et al. (2019)
    """
    const1 = C_LIN["c"] * np.log(C_LIN["v1"] / NGA_EAST_CONSTANTS["vref"])
    const2 = C_LIN["c"] * np.log(C_LIN["v2"] / NGA_EAST_CONSTANTS["vref"])
    f_v = C_LIN["c"] * np.log(vs30 / NGA_EAST_CONSTANTS["vref"])
    f_v[vs30 <= C_LIN["v1"]] = const1
    f_v[vs30 > C_LIN["v2"]] = const2
    idx = vs30 > NGA_EAST_CONSTANTS["vU"]
    if np.any(idx):
        const3 = np.log(3000. / NGA_EAST_CONSTANTS["vU"])
        f_v[idx] = const2 - (const2 + f760[idx]) *\
            (np.log(vs30[idx] / NGA_EAST_CONSTANTS["vU"]) / const3)
    idx = vs30 >= 3000.
    if np.any(idx):
        f_v[idx] = -f760[idx]
    return f_v + f760


def get_fnl(C_NL: Dict, pga_rock: np.ndarray, vs30: np.ndarray, period: float) -> np.ndarray:
    """
    Returns the nonlinear mean amplification according to equation 2
    of Hashash et al. (2019)
    """
    if period <= 0.4:
        vref = 760.
    else:
        vref = 3000.
    f_nl = np.zeros(vs30.shape)
    f_rk = np.log((pga_rock + C_NL["f3"]) / C_NL["f3"])
    idx = vs30 < C_NL["Vc"]
    if np.any(idx):
        # f2 term of the mean nonlinear amplification model
        # according to equation 3 of Hashash et al., (2019)
        c_vs = np.copy(vs30[idx])
        c_vs[c_vs > vref] = vref
        f_2 = C_NL["f4"] * (np.exp(C_NL["f5"] * (c_vs - 360.)) -
                            np.exp(C_NL["f5"] * (vref - 360.)))
        f_nl[idx] = f_2 * f_rk[idx]
    return f_nl, f_rk


def get_linear_stddev(C_LIN: Dict, vs30: np.ndarray):
    """
    Returns the standard deviation of the linear amplification function,
    as defined in equation 4 of Stewart et al., (2019)
    """
    sigma_v = C_LIN["sigma_vc"] + np.zeros(vs30.shape)
    idx = vs30 < C_LIN["vf"]
    if np.any(idx):
        dsig = C_LIN["sigma_L"] - C_LIN["sigma_vc"]
        d_v = (vs30[idx] - NGA_EAST_CONSTANTS["vL"]) /\
            (C_LIN["vf"] - NGA_EAST_CONSTANTS["vL"])
        sigma_v[idx] = C_LIN["sigma_L"] - (2. * dsig * d_v) +\
            dsig * (d_v ** 2.)
    idx = np.logical_and(vs30 > C_LIN["v2"], vs30 <= NGA_EAST_CONSTANTS["vU"])
    if np.any(idx):
        d_v = (vs30[idx] - C_LIN["v2"]) / (NGA_EAST_CONSTANTS["vU"] - C_LIN["v2"])
        sigma_v[idx] = C_LIN["sigma_vc"] + \
            (C_LIN["sigma_U"] - C_LIN["sigma_vc"]) * (d_v ** 2.)
    idx = vs30 >= NGA_EAST_CONSTANTS["vU"]
    if np.any(idx):
        sigma_v[idx] = C_LIN["sigma_U"] *\
            (1. - (np.log(vs30[idx] / NGA_EAST_CONSTANTS["vU"]) /
                   np.log(3000. / NGA_EAST_CONSTANTS["vU"])))
    sigma_v[vs30 > 3000.] = 0.0
    return sigma_v


def get_nonlinear_stddev(C_NL: Dict, vs30: np.ndarray) -> np.ndarray:
    """
    Returns the standard deviation of the nonlinear amplification function,
    as defined in equation 2.5 of Hashash et al. (2017)
    """
    sigma_f2 = np.zeros(vs30.shape)
    sigma_f2[vs30 < 300.] = C_NL["sigma_c"]
    idx = np.logical_and(vs30 >= 300, vs30 < 1000)
    if np.any(idx):
        sigma_f2[idx] = (-C_NL["sigma_c"] / np.log(1000. / 300.)) *\
            np.log(vs30[idx] / 300.) + C_NL["sigma_c"]
    return sigma_f2


# ==========================================================================
# Functions related to the NGA East Standard Deviation Model (Al Atik, 2015)
# ==========================================================================


# Common interpolation function
def ITPL(mag, tu, tl, ml, f):
    return tl + (tu - tl) * (mag - ml) / f


def _scaling(mean_tau, sd_tau2):
    """
    Returns the chi-2 scaling factor from the mean and variance of the
    uncertainty model, as reported in equation 5.4 of Al Atik (2015)
    """
    return sd_tau2 ** 2. / (2.0 * mean_tau ** 2.)


def _dof(mean_tau, sd_tau2):
    """
    Returns the degrees of freedom for the chi-2 distribution from the mean and
    variance of the uncertainty model, as reported in equation 5.5 of Al Atik
    (2015)
    """
    return 2.0 * mean_tau ** 4. / sd_tau2 ** 2.


def _at_percentile(tau, var_tau, percentile):
    """
    Returns the value of the inverse chi-2 distribution at the given
    percentile from the mean and variance of the uncertainty model, as
    reported in equations 5.1 - 5.3 of Al Atik (2015)
    """
    assert percentile >= 0.0 and percentile <= 1.0
    c_val = _scaling(tau, var_tau)
    k_val = _dof(tau, var_tau)
    return np.sqrt(c_val * chi2.ppf(percentile, df=k_val))


# Mean tau values from the global model - Table 5.1
GLOBAL_TAU_MEAN = {
    "PGV": {"tau1": 0.3733, "tau2": 0.3639, "tau3": 0.3434, "tau4": 0.3236},
    "SA": {"tau1": 0.4518, "tau2": 0.4270, "tau3": 0.3863, "tau4": 0.3508}
    }


# Standard deviation of tau values from the global model - Table 5.1
GLOBAL_TAU_SD = {
    "PGV": {"tau1": 0.0558, "tau2": 0.0554, "tau3": 0.0477, "tau4": 0.0449},
    "SA": {"tau1": 0.0671, "tau2": 0.0688, "tau3": 0.0661, "tau4": 0.0491}
}


# Mean tau values from the CENA constant-tau model
CENA_CONSTANT_TAU_MEAN = {"PGV": {"tau": 0.3441}, "SA": {"tau": 0.3695}}


# Standard deviation of tau values from CENA constant-tau model
CENA_CONSTANT_TAU_SD = {"PGV": {"tau": 0.0554}, "SA": {"tau": 0.0688}}


# Mean tau values from the CENA tau model
CENA_TAU_MEAN = {
    "PGV": {"tau1": 0.3477, "tau2": 0.3281, "tau3": 0.3092},
    "SA": {"tau1": 0.3730, "tau2": 0.3375, "tau3": 0.3064}
    }


# Standard deviation of tau values from the CENA tau model
CENA_TAU_SD = {
    "PGV": {"tau1": 0.0554, "tau2": 0.0477, "tau3": 0.0449},
    "SA": {"tau1": 0.0688, "tau2": 0.0661, "tau3": 0.0491}
    }


def global_tau(imt: Union[str, float], mag: np.ndarray, params: Dict) -> np.ndarray:
    """
    'Global' model of inter-event variability, as presented in equation 5.6
    (p103)
    """
    if str(imt).lower() == "pgv":
        C = params["PGV"]
    else:
        C = params["SA"]
    tau = np.full_like(mag, C["tau1"])
    tau[mag > 6.5] = C["tau4"]
    idx = (mag > 5.5) & (mag <= 6.5)
    tau[idx] = ITPL(mag[idx], C["tau4"], C["tau3"], 5.5, 1.0)
    idx = (mag > 5.0) & (mag <= 5.5)
    tau[idx] = ITPL(mag[idx], C["tau3"], C["tau2"], 5.0, 0.5)
    idx = (mag > 4.5) & (mag <= 5.0)
    tau[idx] = ITPL(mag[idx], C["tau2"], C["tau1"], 4.5, 0.5)
    return tau


def cena_constant_tau(imt: Union[str, float], mag: np.ndarray, params: Dict) -> np.ndarray:
    """
    Returns the inter-event tau for the constant tau case
    """
    if imt.string == "PGV":
        return params["PGV"]["tau"]
    else:
        return params["SA"]["tau"]


def cena_tau(imt: Union[str, float], mag: np.ndarray, params: Dict) -> np.ndarray:
    """
    Returns the inter-event standard deviation, tau, for the CENA case
    """
    if imt.string == "PGV":
        C = params["PGV"]
    else:
        C = params["SA"]
    tau = np.full_like(mag, C["tau1"])
    tau[mag > 6.5] = C["tau3"]
    idx = (mag > 5.5) & (mag <= 6.5)
    tau[idx] = ITPL(mag[idx], C["tau3"], C["tau2"], 5.5, 1.0)
    idx = (mag > 5.0) & (mag <= 5.5)
    tau[idx] = ITPL(mag[idx], C["tau2"], C["tau1"], 5.0, 0.5)
    return tau


def get_tau_at_quantile(mean, stddev, quantile):
    """
    Returns the value of tau at a given quantile in the form of a dictionary
    organised by intensity measure
    """
    tau_model = {}
    for imt in mean:
        tau_model[imt] = {}
        for key in mean[imt]:
            if quantile is None:
                tau_model[imt][key] = mean[imt][key]
            else:
                tau_model[imt][key] = _at_percentile(mean[imt][key],
                                                     stddev[imt][key],
                                                     quantile)
    return tau_model


# Gather tau values into dictionary
TAU_SETUP = {
    "cena": {"MEAN": CENA_TAU_MEAN, "STD": CENA_TAU_SD},
    "cena_constant": {"MEAN": CENA_CONSTANT_TAU_MEAN,
                      "STD": CENA_CONSTANT_TAU_SD},
    "global": {"MEAN": GLOBAL_TAU_MEAN, "STD": GLOBAL_TAU_SD}
    }

# Gather tau model implementation functions into dictionary
TAU_EXECUTION = {
    "cena": cena_tau,
    "cena_constant": cena_constant_tau,
    "global": global_tau}


# Phi_ss coefficients for the global model
PHI_SS_GLOBAL = coefficients_to_dictionary("""\
    imt     mean_a   var_a  mean_b  var_b
    pgv     0.5034  0.0609  0.3585  0.0316
    pga     0.5477  0.0731  0.3505  0.0412
    0.010   0.5477  0.0731  0.3505  0.0412
    0.020   0.5464  0.0727  0.3505  0.0416
    0.030   0.5450  0.0723  0.3505  0.0419
    0.040   0.5436  0.0720  0.3505  0.0422
    0.050   0.5424  0.0716  0.3505  0.0425
    0.075   0.5392  0.0707  0.3505  0.0432
    0.100   0.5361  0.0699  0.3505  0.0439
    0.150   0.5299  0.0682  0.3543  0.0453
    0.200   0.5240  0.0666  0.3659  0.0465
    0.250   0.5183  0.0651  0.3765  0.0476
    0.300   0.5127  0.0637  0.3876  0.0486
    0.400   0.5022  0.0611  0.4066  0.0503
    0.500   0.4923  0.0586  0.4170  0.0515
    0.750   0.4704  0.0535  0.4277  0.0526
    1.000   0.4519  0.0495  0.4257  0.0508
    1.500   0.4231  0.0439  0.4142  0.0433
    2.000   0.4026  0.0405  0.4026  0.0396
    3.000   0.3775  0.0371  0.3775  0.0366
    4.000   0.3648  0.0358  0.3648  0.0358
    5.000   0.3583  0.0353  0.3583  0.0356
    7.500   0.3529  0.0350  0.3529  0.0355
    10.00   0.3519  0.0350  0.3519  0.0355
    """)


# Phi_ss coefficients for the CENA model
PHI_SS_CENA = coefficients_to_dictionary("""\
    imt     mean_a   var_a  mean_b   var_b
    pgv     0.5636  0.0807  0.4013  0.0468
    pga     0.5192  0.0693  0.3323  0.0364
    0.010   0.5192  0.0693  0.3323  0.0364
    0.020   0.5192  0.0693  0.3331  0.0365
    0.030   0.5192  0.0693  0.3339  0.0365
    0.040   0.5192  0.0693  0.3348  0.0367
    0.050   0.5192  0.0693  0.3355  0.0367
    0.075   0.5192  0.0693  0.3375  0.0370
    0.100   0.5192  0.0693  0.3395  0.0372
    0.150   0.5192  0.0693  0.3471  0.0382
    0.200   0.5192  0.0693  0.3625  0.0402
    0.250   0.5192  0.0693  0.3772  0.0423
    0.300   0.5192  0.0693  0.3925  0.0446
    0.400   0.5192  0.0693  0.4204  0.0492
    0.500   0.5192  0.0693  0.4398  0.0527
    0.750   0.5192  0.0693  0.4721  0.0590
    1.000   0.5192  0.0693  0.4892  0.0626
    1.500   0.5192  0.0693  0.5082  0.0668
    2.000   0.5192  0.0693  0.5192  0.0693
    3.000   0.5192  0.0693  0.5192  0.0693
    4.000   0.5192  0.0693  0.5192  0.0693
    5.000   0.5192  0.0693  0.5192  0.0693
    7.500   0.5192  0.0693  0.5192  0.0693
    10.00   0.5192  0.0693  0.5192  0.0693
    """)


# Phi_ss coefficients for the CENA constant-phi model
PHI_SS_CENA_CONSTANT = coefficients_to_dictionary("""\
    imt     mean_a    var_a   mean_b   var_b
    pgv     0.5507   0.0678   0.5507  0.0678
    pga     0.5132   0.0675   0.5132  0.0675
    0.010   0.5132   0.0675   0.5132  0.0675
    10.00   0.5132   0.0675   0.5132  0.0675
    """)


# Phi_s2ss coefficients for the CENA
PHI_S2SS_CENA = coefficients_to_dictionary("""\
    imt       mean      var
    pgv     0.4344   0.0200
    pga     0.4608   0.0238
    0.010   0.4608   0.0238
    0.020   0.4617   0.0238
    0.030   0.4700   0.0240
    0.040   0.4871   0.0260
    0.050   0.5250   0.0290
    0.075   0.5800   0.0335
    0.100   0.5930   0.0350
    0.150   0.5714   0.0325
    0.200   0.5368   0.0296
    0.250   0.5058   0.0272
    0.300   0.4805   0.0250
    0.400   0.4440   0.0212
    0.500   0.4197   0.0182
    0.750   0.3849   0.0139
    1.000   0.3667   0.0135
    1.500   0.3481   0.0157
    2.000   0.3387   0.0173
    3.000   0.3292   0.0195
    4.000   0.3245   0.0211
    5.000   0.3216   0.0224
    7.500   0.3178   0.0240
    10.00   0.3159   0.0240
    """)

# Gather the models to setup the phi_ss values for the given quantile
PHI_SETUP = {
    "cena": PHI_SS_CENA,
    "cena_constant": PHI_SS_CENA_CONSTANT,
    "global": PHI_SS_GLOBAL}


def get_phi_ss_at_quantile(phi_model: Dict, quantile: Optional[float] = None) -> Dict:
    """
    Returns the phi_ss values at the specified quantile as an instance of
    `class`:: openquake.hazardlib.gsim.base.CoeffsTable - applies to the
    magnitude-dependent cases
    """
    coeffs = {}
    for imt in phi_model["imt"]:
        phi_imt = get_coefficients_for_imt(phi_model, imt)
        if quantile is None:
            coeffs[imt] = {"a": phi_imt["mean_a"],
                           "b": phi_imt["mean_b"]}
        else:
            coeffs[imt] = {
                "a": _at_percentile(phi_imt["mean_a"],
                                    phi_imt["var_a"],
                                    quantile),
                "b": _at_percentile(phi_imt["mean_b"],
                                    phi_imt["var_b"],
                                    quantile)}
    return coeffs


def get_stewart_2019_phis2s(imt: Union[str, float], vs30: np.ndarray) -> np.ndarray:
    """
    Returns the phis2s model of Stewart et al. (2019)
    """
    v_1 = 1200.
    v_2 = 1500.
    C = get_coefficients_for_imt(COEFFS_USGS_SIGMA_PANEL, imt)
    phis2s = C["s2s1"] + np.zeros(vs30.shape)
    idx = vs30 > v_2
    phis2s[idx] = C["s2s2"]
    idx = np.logical_and(vs30 > v_1, vs30 <= v_2)
    if np.any(idx):
        phis2s[idx] = C["s2s1"] - ((C["s2s1"] - C["s2s2"]) / (v_2 - v_1)) *\
            (vs30[idx] - v_1)
    return phis2s


def get_phi_ss(imt: Union[str, float], mag: np.ndarray, params: Dict) -> np.ndarray:
    """
    Returns the single station phi (or it's variance) for a given magnitude
    and intensity measure type according to equation 5.14 of Al Atik (2015)
    """
    C = params[imt]
    phi = C["a"] + (mag - 5.0) * (C["b"] - C["a"]) / 1.5
    phi[mag <= 5.0] = C["a"]
    phi[mag > 6.5] = C["b"]
    return phi


class ESHM20Craton(GMM):
    """
    Implements the ground motion model of Weatherill & Cotton (2020) for application to the
    cratonic region of Europe

    Weatherill G, Cotton F (2020) "A ground motion logic tree for seismic hazard analysis in
        the stable cratonic region of Europe: regionalisation, model selection and development
        of a scaled backbone approach", Bulletin of Earthquake Engineering, 18:6119 - 6148

    DOI: https://doi.org/10.1007/s10518-020-00940-x

    Attributes:
        epsilon: Number of standard deviations by which to multiple the epistemic uncertainty
                 term (sigma_mu) to be applied to the median ground motion

        site_epsilon: Number of standard deviations by which to multiply the epistemic
                      uncertainty term (sigma_mu,S) to be applied to the site amplification

        ergodic: Apply the ergodic total standard deviation (True) or the non-ergodic
                 total standard deviation (False)

        tau_model: Choice of model for the inter-event standard deviation (tau), selecting
                   from "global" {default}, "cena" or "cena_constant"

        phi_model: Choice of model for the single-station intra-event standard deviation
                   (phi_ss), selecting from "global" {default}, "cena" or "cena_constant"

        tau_quantile: Epistemic uncertainty quantile for the inter-event standard
                      deviation models. Float in the range 0 to 1, or None (mean value used)

        phi_ss_quantile: Epistemic uncertainty quantile for the intra-event standard
                         deviation models. Float in the range 0 to 1, or None (mean value used)

        TAU: Inter-event standard deviation model

        PHI_SS: Single-station standard deviation model
    """
    #: Period Range
    @property
    def period_range(self):
        return (0.01, 10.0)

    @property
    def source_attribs(self):
        # Required source attributes: mag
        return {"mag", }

    @property
    def path_attribs(self):
        # Required distances: rrup
        return {"rrup", }

    @property
    def site_attribs(self):
        # Required site attributes: vs30
        return {"vs30", }

    #: CONSTANTS
    CONSTANTS = {"Mref": 4.5, "Rref": 1., "Mh": 6.2, "h": 5.0}

    #: COEFFICIENTS
    @property
    def COEFFS(self):
        """Returns the coefficients table
        """
        return coefficients_to_dictionary("""\
        imt                    e1                 b1                    b2                  b3                  c1                   c2                    c3            sigma_mu
        pga     0.129433711217154  0.516399476752765   -0.1203218740054820   0.209372712495698   -1.49820100429001    0.220432033342701   -0.2193114966960720   0.467518017234970
        0.010   0.441910295918064  0.507166125004641   -0.1018797167490890   0.184282079939229   -1.56753763950638    0.222961320838036   -0.2173850863710700   0.424145087820724
        0.020   0.979123809125496  0.464490220614734   -0.1137734938103270   0.167233525048116   -1.62825571194736    0.226150925046427   -0.2441521749125150   0.453414267627762
        0.025   1.043340609418350  0.469670674909745   -0.1134508651616400   0.174065913292435   -1.60908830139611    0.224104272434454   -0.2576680445215000   0.456276006752802
        0.030   1.046568495683850  0.476295173341630   -0.1145295451766630   0.188789464775533   -1.57834523952911    0.220697857317202   -0.2700129055991920   0.442617576906802
        0.040   1.007663453495640  0.493809587666455   -0.1150108357853370   0.208535847120219   -1.52232244977795    0.215223039177726   -0.2874767187616130   0.432692547164462
        0.050   0.951568976547282  0.507030793387879   -0.1169997424043950   0.227662857289356   -1.47612267464663    0.210020976504110   -0.2982691158785990   0.436894676747672
        0.075   0.766898926868941  0.537817749890152   -0.1257930384357200   0.255897568366613   -1.39013641948231    0.198935495001160   -0.3062526875169160   0.445048551267241
        0.100   0.566921463821433  0.563265477669262   -0.1390887741365440   0.285966324295526   -1.32905052927637    0.189118846081288   -0.2963709612002850   0.445057073756783
        0.150   0.316925422496063  0.627617718350029   -0.1689678154012890   0.338414772067430   -1.25211993705245    0.167801937655424   -0.2665003749714420   0.408938323358624
        0.200   0.116888680130253  0.691136578143751   -0.1911386191534560   0.377390002770526   -1.20586644897371    0.154400113563626   -0.2365399916865360   0.396717600597790
        0.250  -0.043842379857700  0.744829702492645   -0.2085160327338160   0.406488784261977   -1.18352051545358    0.146981292282198   -0.2083030844596630   0.385803497323193
        0.300  -0.198476724421674  0.799805296458131   -0.2231548236155840   0.433865912912985   -1.16557023447139    0.140633373085703   -0.1797968441826460   0.386776049771811
        0.400  -0.441747369972888  0.897281226627442   -0.2422049150995460   0.483912433515021   -1.15156734492077    0.133979350791855   -0.1362509955087160   0.395064995993542
        0.500  -0.637444825872443  0.992673274984355   -0.2539089461326410   0.526938715295978   -1.14419843291335    0.129943753235505   -0.1121349311669610   0.416676943629526
        0.750  -1.032362404718110  1.237960033431780   -0.2483534410193260   0.613138137400433   -1.12728314803895    0.121478497518643   -0.0735664802614733   0.424883714950325
        1.000  -1.372802902796470  1.445803895497810   -0.2291157391507420   0.691619273496051   -1.10947364377839    0.116810841150476   -0.0583506072267647   0.435248946431388
        1.500  -1.888467249398300  1.730211169117530   -0.1937203497378370   0.805618949392974   -1.10238976578388    0.114304314269286   -0.0390002103787838   0.494395041361088
        2.000  -2.334523112985840  1.920451196131200   -0.1617462515371870   0.908051097334214   -1.09476613327876    0.113858927938807   -0.0296892844443899   0.529656872094865
        3.000  -3.034920080151080  2.146848246139110   -0.1148224554001390   1.085140635646810   -1.09084212215003    0.115716684506372   -0.0198059757373382   0.550851605706151
        4.000  -3.576616283968620  2.262687822224390   -0.0885264828734587   1.227765676724790   -1.09028991715414    0.117770415095847   -0.0135787505915478   0.547911773655132
        5.000  -4.022628827670580  2.318743563950980   -0.0777038034207444   1.346637420710540   -1.09024942151365    0.118983393877196   -0.0083301911092432   0.536941450716745
        7.500  -4.876430881706430  2.373219226144200   -0.0645988540118558   1.529692859278580   -1.10750011821578    0.131643152520841   -0.0000488890402107   0.531853282981450
        10.00  -5.489149076214530  2.381480607871230   -0.0633541563175792   1.620019767639500   -1.12740443208222    0.141291747206530    0.0059559626930461   0.560198970449326
        """)

    def __init__(self, epsilon: float = 0.0, site_epsilon: float = 0.0, ergodic: bool = True,
                 **kwargs):
        """Instantiate the GMM with all the adjustable arguements

        Args:
            epsilon: Number of standard deviations by which to multiply sigma_mu in order
                     to add the epistemic uncertainty on the median
            site_epsilon: Number of standard deviations by which to multiply sigma_mu,S in
                          order to add the epistemic uncertainty on the site amplification
            ergodic: Return the ergodic total standard deviation (True) or the single-station
                     standard deviation (False)
        """
        self.epsilon = epsilon
        self.site_epsilon = site_epsilon
        self.ergodic = ergodic
        self.tau_model = kwargs.get("tau_model", "global")
        self.phi_model = kwargs.get("phi_model", "global")
        self.tau_quantile = kwargs.get("tau_quantile", None)
        self.phi_ss_quantile = kwargs.get("phi_ss_quantile", None)
        self.TAU = get_tau_at_quantile(TAU_SETUP[self.tau_model]["MEAN"],
                                       TAU_SETUP[self.tau_model]["STD"],
                                       self.tau_quantile)
        self.PHI_SS = get_phi_ss_at_quantile(PHI_SETUP[self.phi_model],
                                             self.phi_ss_quantile)

    def __call__(self, scenarios: Union[Dict, np.ndarray, pd.DataFrame], imts: List) ->\
            Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        """Executes the GMM

        Args:
            scenarios: All source, path and site value to be input into the GMM
            imts: List of intensity measure types of either string (for 'pga', 'pgv') or
                  float (for Sa at the specified period)

        Returns:
            medians: The natural logarithm of the expected ground motion
            sigma: The total standard deviation
            tau: The between-event standard deviation
            phi: The within-event standard deviation
        """
        self.requires(scenarios)

        if isinstance(scenarios, dict):
            ngmvs = len(scenarios["mag"])
        else:
            ngmvs = scenarios.shape[0]
        nimts = len(imts)
        medians = np.zeros([ngmvs, nimts])
        tau = np.zeros_like(medians)
        phi = np.zeros_like(medians)
        sigma = np.zeros_like(medians)

        # Get all period-independent terms
        d_m = scenarios["mag"] - self.CONSTANTS["Mh"]
        rval = np.sqrt(scenarios["rrup"] ** 2.0 + self.CONSTANTS["h"] ** 2.)
        rref_val = np.sqrt(self.CONSTANTS["Rref"] ** 2. + self.CONSTANTS["h"] ** 2.)

        # Get PGA on rock
        C_PGA = get_coefficients_for_imt(self.COEFFS, "pga")

        pga_r = np.exp(self.get_magnitude_term(C_PGA, scenarios["mag"], d_m) +
                       self.get_distance_term(C_PGA, scenarios["mag"], rval, rref_val))

        for i, imt in enumerate(imts):
            if str(imt).lower() == "pga":
                median = np.log(pga_r.copy())
                C = copy(C_PGA)
            elif (imt < self.period_range[0]) or (imt > self.period_range[1]):
                print("Period %s outside the supported range %.3f - %.3f"
                      % (str(imt), self.period_range[0], self.period_range[1]))
                medians[:, i] = np.nan
                tau[:, i] = np.nan
                phi[:, i] = np.nan
                sigma[:, i] = np.nan
                continue
            else:
                C = get_coefficients_for_imt(self.COEFFS, imt)
                # Get magnitude scaling term
                median = self.get_magnitude_term(C, scenarios["mag"], d_m) +\
                    self.get_distance_term(C, scenarios["mag"], rval, rref_val)

            median += self.get_site_amplification(imt, pga_r, scenarios["vs30"])
            if self.epsilon:
                medians[:, i] = median + self.epsilon * C["sigma_mu"]
            else:
                medians[:, i] = median
            sigma[:, i], tau[:, i], phi[:, i] = self.get_stddevs(imt, scenarios["mag"],
                                                                 scenarios["vs30"])
        return medians, sigma, tau, phi

    def get_magnitude_term(self, C: Dict, mag: np.ndarray, d_m: np.ndarray) -> np.ndarray:
        """Returns the magnitude scaling term of the model described in equations 1 and 2
        """
        return np.where(mag <= self.CONSTANTS["Mh"],
                        C["e1"] + C["b1"] * d_m + C["b2"] * (d_m ** 2.0),
                        C["e1"] + C["b3"] * d_m)

    def get_distance_term(
            self,
            C: Dict,
            mag: np.ndarray,
            rval: np.ndarray,
            rref_val: np.ndarray
            ) -> np.ndarray:
        """Returns the geometric spreading and anelastic attenuation term of the model,
        described in equations 3 and 4
        """
        return (C["c1"] + C["c2"] * (mag - self.CONSTANTS["Mref"])) *\
            np.log(rval / rref_val) + (C["c3"] * (rval - rref_val) / 100.0)

    def get_site_amplification(self, imt: Union[str, float], pga_r: np.ndarray,
                               vs30: np.ndarray) -> np.ndarray:
        """Returns the sum of the linear (Stewart et al., 2020) and non-linear
        (Hashash et al., 2020) amplification terms
        """
        # Get the coefficients for the IMT
        C_LIN = get_coefficients_for_imt(COEFFS_LINEAR, imt)
        C_F760 = get_coefficients_for_imt(COEFFS_F760, imt)
        C_NL = get_coefficients_for_imt(COEFFS_NONLINEAR, imt)
        if str(imt).lower().startswith("pga"):
            period = 0.01
        elif str(imt).lower().startswith("pgv"):
            period = 0.5
        else:
            period = imt
        # Get f760
        f760 = _get_f760(C_F760, vs30)
        # Get the linear amplification factor
        f_lin = _get_fv(C_LIN, vs30, f760)
        # Get the nonlinear amplification from Hashash et al., (2017)
        f_nl, f_rk = get_fnl(C_NL, pga_r, vs30, period)

        # Mean amplification
        ampl = f_lin + f_nl

        # If an epistemic uncertainty is required then retrieve the epistemic
        # sigma of both models and multiply by the input epsilon
        if self.site_epsilon:
            # In the case of the linear model sigma_f760 and sigma_fv are
            # assumed independent and the resulting sigma_flin is the root
            # sum of squares (SRSS)
            f760_stddev = _get_f760(C_F760, vs30, is_stddev=True)
            f_lin_stddev = np.sqrt(
                f760_stddev ** 2. + get_linear_stddev(C_LIN, vs30) ** 2
                )
            # Likewise, the epistemic uncertainty on the linear and nonlinear
            # model are assumed independent and the SRSS is taken
            f_nl_stddev = get_nonlinear_stddev(C_NL, vs30) * f_rk
            site_epistemic = np.sqrt(f_lin_stddev ** 2. + f_nl_stddev ** 2.)
            ampl += (self.site_epsilon * site_epistemic)
        return ampl

    def get_stddevs(self, imt, mag, vs30):
        """
        Returns the standard deviations for either the ergodic or
        non-ergodic models
        """
        phi = get_phi_ss(imt, mag, self.PHI_SS)
        if self.ergodic:
            phi_s2s = get_stewart_2019_phis2s(imt, vs30)
            phi = np.sqrt(phi ** 2. + phi_s2s ** 2.)
        tau = TAU_EXECUTION[self.tau_model](imt, mag, self.TAU)
        sigma = np.sqrt(tau ** 2. + phi ** 2.)
        return sigma, tau, phi
