"""
Implements the ESHM20 GMM for application to the subduction and deep seismicity regions of
Europe as described in Weatherill et al. (2023) [Draft]

"""
from typing import Union, Dict, List, Optional, Tuple
import numpy as np
import pandas as pd
from gmms.base import coefficients_to_dictionary, get_coefficients_for_imt, GMM


BCHYDRO_SIGMA_MU = coefficients_to_dictionary("""\
    imt     SIGMA_MU_SINTER    SIGMA_MU_SSLAB
    pga                 0.3              0.50
    0.010               0.3              0.50
    0.020               0.3              0.50
    0.030               0.3              0.50
    0.050               0.3              0.50
    0.075               0.3              0.50
    0.100               0.3              0.50
    0.150               0.3              0.50
    0.200               0.3              0.50
    0.250               0.3              0.46
    0.300               0.3              0.42
    0.400               0.3              0.38
    0.500               0.3              0.34
    0.600               0.3              0.30
    0.750               0.3              0.30
    1.000               0.3              0.30
    1.500               0.3              0.30
    2.000               0.3              0.30
    2.500               0.3              0.30
    3.000               0.3              0.30
    4.000               0.3              0.30
    5.000               0.3              0.30
    6.000               0.3              0.30
    7.500               0.3              0.30
    10.00               0.3              0.30
    """)


COEFFS_MAG_SCALE_SINTER = {
    "low": coefficients_to_dictionary("""\
                                      imt    dc1
                                      pga    0.0
                                      0.02   0.0
                                      0.30   0.0
                                      0.50  -0.1
                                      1.00  -0.2
                                      2.00  -0.3
                                      3.00  -0.4
                                      10.0  -0.4
                                      """),
    "mid": coefficients_to_dictionary("""\
                                      imt    dc1
                                      pga    0.2
                                      0.02   0.2
                                      0.30   0.2
                                      0.50   0.1
                                      1.00   0.0
                                      2.00  -0.1
                                      3.00  -0.2
                                      10.0  -0.2
                                      """),
    "high": coefficients_to_dictionary("""\
                                       imt    dc1
                                       pga    0.4
                                       0.02   0.4
                                       0.30   0.4
                                       0.50   0.3
                                       1.00   0.2
                                       2.00   0.1
                                       3.00   0.0
                                       10.0   0.0
                                       """),
    }


COEFFS_MAG_SCALE_SSLAB = {"low": -0.5, "mid": -0.3, "high": -0.1}


def faba_taper_sfunction(x: np.ndarray, a: float = 0.0, b: float = 0.0) -> np.ndarray:
    """
    Returns a taper based on an S-Function

    Args:
        x: Input array
        a: Controlling shape parameter (a)
        b: Controlling shape parameter (b)

    Returns:
        y: Output array
    """
    y = np.ones(x.shape)
    idx = x <= a
    y[idx] = 0

    idx = np.logical_and(a <= x, x <= (a + b) / 2.)
    y[idx] = 2. * ((x[idx] - a) / (b - a)) ** 2.

    idx = np.logical_and((a + b) / 2. <= x, x <= b)
    y[idx] = 1 - 2. * ((x[idx] - b) / (b - a)) ** 2.
    return y


def faba_taper_step(x: np.ndarray) -> np.ndarray:
    """Returns a taper based on a step function
    """
    y = np.zeros(x.shape)
    y[x > 0.0] = 1.0
    return y


class ESHM20SInter(GMM):
    """
    Implements the ESHM20 ground motion model for application to subduction interface regions
    as described in Weatherill et al. (2023).

    Weatherill G, Kotha S R, Cotton F, Bindi D, Danciu L, Vilanova S (2023) "Modelling Ground
        Motion for the 2020 European Seismic Hazard Model (ESHM20): Logic Tree Overview and
        Implementation", submitted to Natural Hazards and Earth System Sciences

    Attributes:
        sigma_mu_epsilon: Number of standard deviations by which to multiple the epistemic
                          uncertainty term (sigma_mu) to be applied to the median ground motion

        theta6_adjustment: Number to increase or decrease the anelastic attenuation coefficient
                           theta6 in order to capture epistemic uncertainty in anelastic
                           attenuation (recommend either 0 [default] or +/- 0.0015)

        ergodic: Apply the ergodic total standard deviation (True) or the non-ergodic
                 total standard deviation (False)

        dc1_coeffs: Magnitude scaling adjustment term DeltaC1

        a_taper: a-coefficient (in km) of S-function taper for Forearc/Backarc scaling

        b_taper: b-coefficient (in km) of S-function taper for Forearc/Backarc scaling

        large_magnitude_scaling: Determines whether to apply the "low", "mid" [default] or
                                 "high" branch of the large-magnitude scaling (not used in
                                 ESHM20)
        rkey: Indicates the key to use for distance depending on whether the GMM is for
              interface events ("rrup") or in-slab events("rhypo")
    """
    @property
    def period_range(self):
        return (0.02, 10.0)

    @property
    def source_attribs(self):
        # Required source attributes: mag
        return {"mag", }

    @property
    def path_attribs(self):
        # Required distances: rrup
        return {"rrup", }

    @property
    def site_attribs(self):
        # Required site attributes: vs30, xvf
        return {"vs30", "xvf", }

    #: CONSTANTS
    CONSTANTS = {'n': 1.18, 'c': 1.88, 'theta3': 0.1, 'theta4': 0.9,
                 'theta5': 0.0, 'theta9': 0.4, 'C4': 10.0, 'C1': 7.8}

    @property
    def fevent(self):
        return 0

    #: COEFFICIENTS
    @property
    def COEFFS(self):
        """Returns the coefficients table
        """
        return coefficients_to_dictionary("""\
        imt          vlin        b   theta1    theta2        theta6    theta7    theta8  theta10  theta11   theta12   theta13   theta14  theta15   theta16      phi     tau   sigma  sigma_ss
        pga      865.1000  -1.1860   4.2203   -1.3500   -0.00721467    1.0988   -1.4200   3.1200   0.0130    0.9800   -0.0135   -0.4000   0.9969   -1.0000   0.6000  0.4300  0.7400    0.6000
        0.0200   865.1000  -1.1860   4.2203   -1.3500   -0.00719296    1.0988   -1.4200   3.1200   0.0130    0.9800   -0.0135   -0.4000   0.9969   -1.0000   0.6000  0.4300  0.7400    0.6000
        0.0500  1053.5000  -1.3460   4.5371   -1.4000   -0.00712619    1.2536   -1.6500   3.3700   0.0130    1.2880   -0.0138   -0.4000   1.1030   -1.1800   0.6000  0.4300  0.7400    0.6000
        0.0750  1085.7000  -1.4710   5.0733   -1.4500   -0.00701600    1.4175   -1.8000   3.3700   0.0130    1.4830   -0.0142   -0.4000   1.2732   -1.3600   0.6000  0.4300  0.7400    0.6000
        0.1000  1032.5000  -1.6240   5.2892   -1.4500   -0.00687258    1.3997   -1.8000   3.3300   0.0130    1.6130   -0.0145   -0.4000   1.3042   -1.3600   0.6000  0.4300  0.7400    0.6000
        0.1500   877.6000  -1.9310   5.4563   -1.4500   -0.00671633    1.3582   -1.6900   3.2500   0.0130    1.8820   -0.0153   -0.4000   1.2600   -1.3000   0.6000  0.4300  0.7400    0.6000
        0.2000   748.2000  -2.1880   5.2684   -1.4000   -0.00657867    1.1648   -1.4900   3.0300   0.0129    2.0760   -0.0162   -0.3500   1.2230   -1.2500   0.6000  0.4300  0.7400    0.6000
        0.2500   654.3000  -2.3810   5.0594   -1.3500   -0.00648602    0.9940   -1.3000   2.8000   0.0129    2.2480   -0.0172   -0.3100   1.1600   -1.1700   0.6000  0.4300  0.7400    0.6000
        0.3000   587.1000  -2.5180   4.7945   -1.2800   -0.00643709    0.8821   -1.1800   2.5900   0.0128    2.3480   -0.0183   -0.2800   1.0500   -1.0600   0.6000  0.4300  0.7400    0.6000
        0.4000   503.0000  -2.6570   4.4644   -1.1800   -0.00639138    0.7046   -0.9800   2.2000   0.0127    2.4270   -0.0206   -0.2300   0.8000   -0.7800   0.6000  0.4300  0.7400    0.6000
        0.5000   456.6000  -2.6690   4.0181   -1.0800   -0.00629147    0.5799   -0.8200   1.9200   0.0125    2.3990   -0.0231   -0.1900   0.6620   -0.6200   0.6000  0.4300  0.7400    0.6000
        0.6000   430.3000  -2.5990   3.6055   -0.9900   -0.00609857    0.5021   -0.7000   1.7000   0.0124    2.2730   -0.0256   -0.1600   0.5800   -0.5000   0.6000  0.4300  0.7400    0.6000
        0.7500   410.5000  -2.4010   3.2174   -0.9100   -0.00581454    0.3687   -0.5400   1.4200   0.0120    1.9930   -0.0296   -0.1200   0.4800   -0.3400   0.6000  0.4300  0.7400    0.6000
        1.0000   400.0000  -1.9550   2.7981   -0.8500   -0.00548905    0.1746   -0.3400   1.1000   0.0114    1.4700   -0.0363   -0.0700   0.3300   -0.1400   0.6000  0.4300  0.7400    0.6000
        1.5000   400.0000  -1.0250   2.0123   -0.7700   -0.00520499   -0.0820   -0.0500   0.7000   0.0100    0.4080   -0.0493    0.0000   0.3100    0.0000   0.6000  0.4300  0.7400    0.6000
        2.0000   400.0000  -0.2990   1.4128   -0.7100   -0.00505022   -0.2821    0.1200   0.7000   0.0085   -0.4010   -0.0610    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        2.5000   400.0000   0.0000   0.9976   -0.6700   -0.00507967   -0.4108    0.2500   0.7000   0.0069   -0.7230   -0.0711    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        3.0000   400.0000   0.0000   0.6443   -0.6400   -0.00529221   -0.4466    0.3000   0.7000   0.0054   -0.6730   -0.0798    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        4.0000   400.0000   0.0000   0.0657   -0.5800   -0.00564790   -0.4344    0.3000   0.7000   0.0027   -0.6270   -0.0935    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        5.0000   400.0000   0.0000  -0.4624   -0.5400   -0.00607621   -0.4368    0.3000   0.7000   0.0005   -0.5960   -0.0980    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        6.0000   400.0000   0.0000  -0.9809   -0.5000   -0.00647922   -0.4586    0.3000   0.7000  -0.0013   -0.5660   -0.0980    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        7.5000   400.0000   0.0000  -1.6017   -0.4600   -0.00676355   -0.4433    0.3000   0.7000  -0.0033   -0.5280   -0.0980    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        10.0000  400.0000   0.0000  -2.2937   -0.4000   -0.00686566   -0.4828    0.3000   0.7000  -0.0060   -0.5040   -0.0980    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        """)

    def __init__(self, sigma_mu_epsilon: float = 0.0, theta6_adjustment: float = 0.0,
                 ergodic: bool = True, a: Optional[float] = None, b: Optional[float] = None,
                 large_magnitude_scaling: Optional[str] = None):
        """
        Args:
            sigma_mu_epsilon: Number of standard deviations by which to multiple the epistemic
                              uncertainty term (sigma_mu) to be applied to the median ground
                              motion

            theta6_adjustment: Number to increase or decrease the anelastic attenuation
                               coefficient theta6 in order to capture epistemic uncertainty in
                               anelastic attenuation (recommend either 0 [default] or
                               +/-0.0015)

            ergodic: Apply the ergodic total standard deviation (True) or the non-ergodic
                     total standard deviation (False)

             a: a-coefficient of S-function taper for Forearc/Backarc scaling (default = None)

             b: b-coefficient of S-function taper for Forearc/Backarc scaling (default = Nome)

             large_magnitude_scaling: Determines whether to apply the "low", "mid" [default] or
                                      "high" branch of the large-magnitude scaling (not used in
                                      ESHM20)
        """
        self.sigma_mu_epsilon = sigma_mu_epsilon
        self.theta6_adjustment = theta6_adjustment
        self.ergodic = ergodic
        self.dc1_coeffs = self.get_dc1(large_magnitude_scaling)
        self.a_taper = a
        self.b_taper = b
        if self.fevent:
            self.rkey = "rhypo"
        else:
            self.rkey = "rrup"

    def __call__(self, scenarios: Union[Dict, np.ndarray, pd.DataFrame], imts: List) ->\
            Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        """Executes the GMM

        Args:
            scenarios: All source, path and site value to be input into the GMM
            imts: List of intensity measure types of either string (for 'pga', 'pgv') or
                  float (for Sa at the specified period)

        Returns:
            medians: The natural logarithm of the expected ground motion
            sigma: The total standard deviation
            tau: The between-event standard deviation
            phi: The within-event standard deviation
        """
        self.requires(scenarios)
        if isinstance(scenarios, dict):
            ngmvs = len(scenarios["mag"])
        else:
            ngmvs = scenarios.shape[0]
        nimts = len(imts)
        medians = np.zeros([ngmvs, nimts])
        tau = np.zeros_like(medians)
        phi = np.zeros_like(medians)
        sigma = np.zeros_like(medians)
        # Get the forearc/backarc taper (independent of IMT)
        if self.a_taper and self.b_taper:
            faba_taper = faba_taper_sfunction(-scenarios["xvf"], self.a_taper, self.b_taper)
        else:
            faba_taper = faba_taper_step(-scenarios["xvf"])

        # Get PGA on rock
        pga1000 = self.get_pga_rock(scenarios, faba_taper)
        for i, imt in enumerate(imts):
            if isinstance(imt, float) and ((imt < self.period_range[0]) or
                                           (imt > self.period_range[1])):
                # Outside the period range - skipping
                print("Period %s outside the supported range %.3f - %.3f"
                      % (str(imt), self.period_range[0], self.period_range[1]))
                # Set to NaN
                medians[:, i] = np.nan
                tau[:, i] = np.nan
                phi[:, i] = np.nan
                sigma[:, i] = np.nan
                continue
            C = get_coefficients_for_imt(self.COEFFS, imt)
            median = self.get_median(C, scenarios, imt, faba_taper, pga1000)
            if self.sigma_mu_epsilon:
                medians[:, i] = median + self.get_stress_adjustment(imt)
            else:
                medians[:, i] = median
            sigma_i, tau_i, phi_i = self.get_stddevs(C)
            sigma[:, i] += sigma_i
            tau[:, i] += tau_i
            phi[:, i] += phi_i
        return medians, sigma, tau, phi

    def get_dc1(self, large_magnitude_scaling: Optional[str] = None) -> Union[Dict, float]:
        """Returns the epistemic uncertainty coefficient DeltaC1, which takes the
        form of a imt-dependent dictionary for interface events, or a float for in-slab events
        """
        large_mag_scaling = large_magnitude_scaling if large_magnitude_scaling else "mid"
        assert large_mag_scaling in ("low", "mid", "high"),\
            "Large magnitude scaling term %s not supported (must be 'low', 'mid' or 'high')" %\
            large_mag_scaling
        if self.fevent:
            return COEFFS_MAG_SCALE_SSLAB[large_mag_scaling]
        else:
            return COEFFS_MAG_SCALE_SINTER[large_mag_scaling]

    def get_median(self, C: Dict, scenarios, imt: Union[float, str], faba_taper: np.ndarray,
                   pga1100: np.ndarray) -> np.ndarray:
        """Returns the median ground motion for the given scenarios and intensity measure

        Args:
            faba_taper: Forearc/backarc taper
            pga1100: PGA (g) on 1000 m/s rock
        """
        dc1 = self.dc1_coeffs if self.fevent else\
            get_coefficients_for_imt(self.dc1_coeffs, imt)["dc1"]
        if self.fevent:
            f_depth = C["theta11"] * (np.clip(scenarios["hypo_depth"], -np.inf, 120.0) - 60.0)
        else:
            f_depth = 0.0
        return (
            self.get_magnitude_term(C, scenarios["mag"], dc1) +
            self.get_geometric_spreading_term(C, scenarios["mag"], scenarios[self.rkey]) +
            self.get_anelastic_attenuation_term(C, scenarios[self.rkey]) + f_depth +
            self.get_forearc_backarc_scaling_term(C, scenarios[self.rkey], faba_taper) +
            self.get_site_amplification_term(C, scenarios["vs30"], pga1100)
            )

    def get_stress_adjustment(self, imt: Union[float, str]) -> np.ndarray:
        """Returns the stress adjustment epistemic uncertainty
        """
        C_A = get_coefficients_for_imt(BCHYDRO_SIGMA_MU, imt)
        sigma_mu = (C_A["SIGMA_MU_SSLAB"] if self.fevent else C_A["SIGMA_MU_SINTER"]) / 1.65
        return sigma_mu * self.sigma_mu_epsilon

    def get_pga_rock(self, scenarios, faba_taper: np.ndarray) -> np.ndarray:
        """Gets the PGA on reference rock Vs30 1000 m/s
        """
        C_PGA = get_coefficients_for_imt(self.COEFFS, "pga")
        dc1 = self.dc1_coeffs if self.fevent else get_coefficients_for_imt(self.dc1_coeffs,
                                                                           "pga")["dc1"]
        n = scenarios["vs30"].shape
        vs30 = 1000.0 * np.ones(n)
        if self.fevent:
            f_depth = C_PGA["theta11"] * (np.clip(scenarios["hypo_depth"], -np.inf, 120) - 60.0)
        else:
            f_depth = 0.0
        return np.exp(
            self.get_magnitude_term(C_PGA, scenarios["mag"], dc1) +
            self.get_geometric_spreading_term(C_PGA, scenarios["mag"], scenarios[self.rkey]) +
            self.get_anelastic_attenuation_term(C_PGA, scenarios[self.rkey]) + f_depth +
            self.get_forearc_backarc_scaling_term(C_PGA, scenarios[self.rkey], faba_taper) +
            self.get_site_amplification_term(C_PGA, vs30, 0.0)
        )

    def get_magnitude_term(self, C: Dict, mag: np.ndarray, dc1: float) -> np.ndarray:
        """Returns the stress coefficient and magnitude scaling term described in
        Equation 2 of Abrahamson et al. (2016)
        """
        dmag = self.CONSTANTS["C1"] + dc1
        f_stress = C["theta1"] + self.CONSTANTS["theta4"] * dc1
        f_m = np.where(
            mag <= dmag,
            (self.CONSTANTS["theta4"] * (mag - dmag)) + C["theta13"] * ((10.0 - mag) ** 2.),
            (self.CONSTANTS["theta5"] * (mag - dmag)) + C["theta13"] * ((10.0 - mag) ** 2.)
            )
        return f_stress + f_m

    def get_geometric_spreading_term(self, C: Dict, mag: np.ndarray, rrup: np.ndarray) ->\
            np.ndarray:
        """Returns the geometric spreading term contained within equation 1a of
        Abrahamson et al. (2016)
        """
        if self.fevent:
            theta14 = C["theta14"]
        else:
            theta14 = 0.0
        return (C["theta2"] + theta14 + self.CONSTANTS["theta3"] * (mag - 7.8)) *\
            np.log(rrup + self.CONSTANTS["C4"] * np.exp(self.CONSTANTS["theta9"] * (mag - 6.0)))

    def get_anelastic_attenuation_term(self, C: Dict, rval: np.ndarray) -> np.ndarray:
        """Returns the anelastic attenuation term (applying the adjustment)
        """
        fdist = (C["theta6"] + self.theta6_adjustment) * rval
        if self.fevent:
            # For inslab events - need to add additional coefficient
            fdist += C["theta10"]
        return fdist

    def get_forearc_backarc_scaling_term(self, C: Dict, rval: np.ndarray,
                                         faba_taper: np.ndarray) -> np.ndarray:
        """Returns the forearc/backarc scaling term described in Equation 4 in Abrahamson et al
        (2016)
        """
        if self.fevent:
            # Inslab
            ffaba = C["theta7"] + C["theta8"] * np.log(np.clip(rval, 85.0, np.inf) / 40.0)
        else:
            # Interface
            ffaba = C["theta15"] + C["theta16"] * np.log(np.clip(rval, 100.0, np.inf) / 40.0)
        return ffaba * faba_taper

    def get_site_amplification_term(self, C: Dict, vs30: np.ndarray,
                                    pga1100: Union[np.ndarray, float]) -> np.ndarray:
        """Returns the site amplifiction term described in equations 5 and 6 of Abrahamson et
        al (2016)
        """
        vs_star = np.clip(vs30, -np.inf, 1000.0) / C["vlin"]
        f_lin = C["theta12"] * np.log(vs_star)
        f_nl = np.where(
            vs30 >= C["vlin"],
            C["b"] * self.CONSTANTS["n"] * np.log(vs_star),
            C["b"] * np.log(pga1100 + self.CONSTANTS["c"] * (vs_star ** self.CONSTANTS["n"])) -
            C["b"] * np.log(pga1100 + self.CONSTANTS["c"])
        )
        return f_lin + f_nl

    def get_stddevs(self, C: Dict):
        """Returns the inter-event, within-event and total standard devation
        """
        tau = C["tau"]
        if self.ergodic:
            phi = C["phi"]
            sigma = C["sigma"]
            # sigma = np.sqrt(tau ** 2. + C["phi"] ** 2.)
        else:
            sigma = C["sigma_ss"]
            phi = np.sqrt(C["sigma_ss"] ** 2.0 - tau ** 2.0)
        return sigma, tau, phi


class ESHM20SSlab(ESHM20SInter):
    """
    Implements the ESHM20 ground motion model for application to subduction in-slab and deep
    seismicity regions, as described in Weatherill et al. (2023).
    """
    @property
    def source_attribs(self):
        # Required source attributes: mag, hypo_depth
        return {"mag", "hypo_depth"}

    @property
    def path_attribs(self):
        # Required distances: rhypo
        return {"rhypo", }

    @property
    def fevent(self):
        return 1

    #: COEFFICIENTS
    @property
    def COEFFS(self):
        """Returns the coefficients table (note the adjusted theta6 term)
        """
        return coefficients_to_dictionary("""\
        imt          vlin        b   theta1    theta2        theta6    theta7    theta8  theta10  theta11   theta12   theta13   theta14  theta15   theta16      phi     tau   sigma  sigma_ss
        pga      865.1000  -1.1860   4.2203   -1.3500   -0.00278801    1.0988   -1.4200   3.1200   0.0130    0.9800   -0.0135   -0.4000   0.9969   -1.0000   0.6000  0.4300  0.7400    0.6000
        0.0200   865.1000  -1.1860   4.2203   -1.3500   -0.00275821    1.0988   -1.4200   3.1200   0.0130    0.9800   -0.0135   -0.4000   0.9969   -1.0000   0.6000  0.4300  0.7400    0.6000
        0.0500  1053.5000  -1.3460   4.5371   -1.4000   -0.00268517    1.2536   -1.6500   3.3700   0.0130    1.2880   -0.0138   -0.4000   1.1030   -1.1800   0.6000  0.4300  0.7400    0.6000
        0.0750  1085.7000  -1.4710   5.0733   -1.4500   -0.00261360    1.4175   -1.8000   3.3700   0.0130    1.4830   -0.0142   -0.4000   1.2732   -1.3600   0.6000  0.4300  0.7400    0.6000
        0.1000  1032.5000  -1.6240   5.2892   -1.4500   -0.00259240    1.3997   -1.8000   3.3300   0.0130    1.6130   -0.0145   -0.4000   1.3042   -1.3600   0.6000  0.4300  0.7400    0.6000
        0.1500   877.6000  -1.9310   5.4563   -1.4500   -0.00264688    1.3582   -1.6900   3.2500   0.0130    1.8820   -0.0153   -0.4000   1.2600   -1.3000   0.6000  0.4300  0.7400    0.6000
        0.2000   748.2000  -2.1880   5.2684   -1.4000   -0.00277703    1.1648   -1.4900   3.0300   0.0129    2.0760   -0.0162   -0.3500   1.2230   -1.2500   0.6000  0.4300  0.7400    0.6000
        0.2500   654.3000  -2.3810   5.0594   -1.3500   -0.00296427    0.9940   -1.3000   2.8000   0.0129    2.2480   -0.0172   -0.3100   1.1600   -1.1700   0.6000  0.4300  0.7400    0.6000
        0.3000   587.1000  -2.5180   4.7945   -1.2800   -0.00318216    0.8821   -1.1800   2.5900   0.0128    2.3480   -0.0183   -0.2800   1.0500   -1.0600   0.6000  0.4300  0.7400    0.6000
        0.4000   503.0000  -2.6570   4.4644   -1.1800   -0.00340820    0.7046   -0.9800   2.2000   0.0127    2.4270   -0.0206   -0.2300   0.8000   -0.7800   0.6000  0.4300  0.7400    0.6000
        0.5000   456.6000  -2.6690   4.0181   -1.0800   -0.00363798    0.5799   -0.8200   1.9200   0.0125    2.3990   -0.0231   -0.1900   0.6620   -0.6200   0.6000  0.4300  0.7400    0.6000
        0.6000   430.3000  -2.5990   3.6055   -0.9900   -0.00388267    0.5021   -0.7000   1.7000   0.0124    2.2730   -0.0256   -0.1600   0.5800   -0.5000   0.6000  0.4300  0.7400    0.6000
        0.7500   410.5000  -2.4010   3.2174   -0.9100   -0.00415403    0.3687   -0.5400   1.4200   0.0120    1.9930   -0.0296   -0.1200   0.4800   -0.3400   0.6000  0.4300  0.7400    0.6000
        1.0000   400.0000  -1.9550   2.7981   -0.8500   -0.00445479    0.1746   -0.3400   1.1000   0.0114    1.4700   -0.0363   -0.0700   0.3300   -0.1400   0.6000  0.4300  0.7400    0.6000
        1.5000   400.0000  -1.0250   2.0123   -0.7700   -0.00478084   -0.0820   -0.0500   0.7000   0.0100    0.4080   -0.0493    0.0000   0.3100    0.0000   0.6000  0.4300  0.7400    0.6000
        2.0000   400.0000  -0.2990   1.4128   -0.7100   -0.00513159   -0.2821    0.1200   0.7000   0.0085   -0.4010   -0.0610    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        2.5000   400.0000   0.0000   0.9976   -0.6700   -0.00550694   -0.4108    0.2500   0.7000   0.0069   -0.7230   -0.0711    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        3.0000   400.0000   0.0000   0.6443   -0.6400   -0.00590809   -0.4466    0.3000   0.7000   0.0054   -0.6730   -0.0798    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        4.0000   400.0000   0.0000   0.0657   -0.5800   -0.00634283   -0.4344    0.3000   0.7000   0.0027   -0.6270   -0.0935    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        5.0000   400.0000   0.0000  -0.4624   -0.5400   -0.00680074   -0.4368    0.3000   0.7000   0.0005   -0.5960   -0.0980    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        6.0000   400.0000   0.0000  -0.9809   -0.5000   -0.00722208   -0.4586    0.3000   0.7000  -0.0013   -0.5660   -0.0980    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        7.5000   400.0000   0.0000  -1.6017   -0.4600   -0.00752097   -0.4433    0.3000   0.7000  -0.0033   -0.5280   -0.0980    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        10.0000  400.0000   0.0000  -2.2937   -0.4000   -0.00762908   -0.4828    0.3000   0.7000  -0.0060   -0.5040   -0.0980    0.0000   0.3000    0.0000   0.6000  0.4300  0.7400    0.6000
        """)
