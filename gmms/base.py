"""
Utility functions for the ground motion models
"""
import abc
from typing import Dict, Union, List, Tuple
import numpy as np
import pandas as pd


eshm20_dtypes = {
    "mag": np.float64,
    "hypo_depth": np.float64,
    "rjb": np.float64,
    "rrup": np.float64,
    "vs30": np.float64,
    "vs30measured": bool,
    "region": np.uint32,
    "xvf": np.float64,
    "geology": (np.string_, 20),
}


def coefficients_to_dictionary(coeffs: str) -> Dict:
    """
    Translate the coefficients table from a string to a dictionary ordered by
    intensity measure type
    """
    output = {}
    keys = []
    for i, row in enumerate(coeffs.split('\n')):
        if not row:
            continue
        if not keys:
            keys = row.split()
            for key in keys:
                output[key] = []
        else:
            for j, val in enumerate(row.split()):
                if val not in ("pgv", "pga"):
                    output[keys[j]].append(float(val))
                else:
                    output[keys[j]].append(val)
    for key in keys:
        if key not in "imt":
            output[key] = np.array(output[key])
    return output


def get_coefficients_for_imt(coeffs: Dict, imt: Union[str, float]) -> Dict:
    """
    Retrieves the coefficients specific to a given IMT
    """
    # Get coefficients for imt
    if imt in ("pgv", "pga"):
        idx = coeffs["imt"].index(imt)
        C = {}
        for key in coeffs:
            C[key] = coeffs[key][idx]
    elif isinstance(imt, float):
        sa_coeffs = {"imt": []}
        fidx = []
        for i, val in enumerate(coeffs["imt"]):
             if isinstance(val, float):
                 fidx.append(i)
                 sa_coeffs["imt"].append(val)
        sa_coeffs["imt"] = np.array(sa_coeffs["imt"])
        for key in coeffs:
            if key != "imt":
                sa_coeffs[key] = coeffs[key][fidx]
        if (imt < sa_coeffs['imt'][0]) | (imt > sa_coeffs["imt"][-1]):
            # Interpolation is allowed, but not extrapolation - raise error
            raise ValueError("Period %s outside supported period range (%s to %s)" %
                             (str(imt), str(sa_coeffs["imt"][0]), str(sa_coeffs["imt"][-1])))
        sa_coeffs_imt = np.log(sa_coeffs["imt"])
        C = {}
        for key in sa_coeffs:
            if key != "imt":
                C[key] = np.interp(np.log(imt), sa_coeffs_imt, sa_coeffs[key])
    else:
        raise ValueError("IMT %s not recognised" % str(imt))
    return C


class GMM(abc.ABC):
    """
    Abstract Base Class for a Ground Motion Model (GMM)
    """
    @property
    @abc.abstractmethod
    def period_range(self):
        return ()

    @property
    @abc.abstractmethod
    def source_attribs(self):
        """Returns the list of required source parameters
        """
        return {"mag", }

    @property
    @abc.abstractmethod
    def path_attribs(self):
        """Returns the list of required path (distance) parameters
        """
        return set()

    @property
    @abc.abstractmethod
    def site_attribs(self):
        """Returns the list of required site parameters
        """
        return set()

    # If the GMM has constants then these should be defined here
    CONSTANTS = abc.abstractproperty()

    @property
    @abc.abstractmethod
    def COEFFS(self):
        """Definets the coefficients table
        """
        pass

    @abc.abstractmethod
    def __call__(self, scenarios: Union[Dict, np.ndarray, pd.DataFrame], imts: List) ->\
            Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        """Executes the main call to the GMM
        """
        pass

    def requires(self, scenarios: Union[Dict, np.ndarray, pd.DataFrame]):
        """Checks that the required source, path and site data are in the inputs or raises
        a ValueError if not
        """
        for key in set(self.source_attribs | self.path_attribs | self.site_attribs):
            if isinstance(scenarios, dict) and key not in scenarios:
                raise ValueError("Scenarios missing required parameter %s" % key)
            elif isinstance(scenarios, np.ndarray) and key not in scenarios.dtype.names:
                raise ValueError("Scenarios missing required parameter %s" % key)
            elif isinstance(scenarios, pd.DataFrame) and key not in scenarios.columns:
                raise ValueError("Scenarios missing required parameter %s" % key)
            else:
                pass
        return
