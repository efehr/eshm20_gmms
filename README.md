# ESHM20 Ground Motion Models - Python Module

(c) Graeme Weatherill, GFZ German Research Centre for Geosciences, Potsdam, Germany

Email: [graeme.weatherill@gfz-potsdam.de](graeme.weatherill@gfz-potsdam.de) 

### Contributors
Graeme Weatherill, GFZ German Research Centre for Geosciences, Potsdam, Germany

## Overview

In this repository we include a lightweight implementation of the Ground Motion Models (GMMs) used for the 2020 European Seismic Hazard Model (ESHM20) (Danciu et al., 2021) and 2020 European Seismic Risk Model (ESRM20) (Crowley et al., 2021). The software can be installed as a Python module (see [Installation] (#Installation) or can serve as a template for users own implementations of the models.

The GMMs contain all of the implementation decisions and adjustments that were used for the ESHM20. In this sense, they may differ from implementations of the GMMs in their original publications of Kotha et al. (2020) and Weatherill et al. (2020) and instead contain the changes described in Danciu et al. (2021), Weatherill et al. (2023) and Weatherill et al. (2024). For full information on these please consult the [References](#References) below.

The _authoritative_ implementation of the models can be found in the GMM Library of the [OpenQuake-engine](https://github.com/gem/oq-engine); however the models here follow the original implementations closely and are tested against the same test-tables used by OpenQuake. Further explanation of how to run the GMMs in the current tools can be found in the [Usage](#Usage) section, and are illustrated in the attached Jupyter Notebook.


## Installation
The modules are intended for use in Python 3.8 or later, with a minimal number of Python dependencies. Currently these are limited to:

```
Numpy
Scipy
Pandas
Matplotlib
```
For the Jupyter Notebooks it will be necessary to install:

```
Jupyter
```

It is recommended to use a Python Virtual Environment - please see here for instructions on setting up and running a virtual environment (https://docs.python.org/3/library/venv.html)

To install the Python module we recommend downloading or cloning the code from the Git repository via:

```
git clone https://gitlab.seismo.ethz.ch/efehr/eshm20_gmms.git
```

Then `cd` into the directory `eshm20_gmms` and install via the `setup.py` file and Pip Python Package Manager

```
pip install -e .
```

For users interested in developing the code we recommend the additional modules

```
pylint
flake8
pytest
pytest-cov
```
which can be installed with the optional `[dev]` flag:

```
pip install -e .[dev]
```

## Ground Motion Model (GMM) Details

The GMM logic tree of the ESHM20 and ESRM20 is a scaled backbone model, meaning that each set of branches will be built around a single GMM (for the given tectonic region type) that is adjusted according to the factors described in Weatherill et al. (2020; 2023; 2024) and Danciu et al. (2021). The factors aim to describe the epistemic uncertainty in source/stress parameter and in anelastic (or residual) attenuation. 

### Adjustment Factors

Throughout the whole logic tree the GMMs are usually adjusted according to factors (and their corresponding weights) that represent discrete approximations to a standard normal distribution, defined using the Gaussian Quadrature approach of Miller & Rice (1983). In the final ESHM20 logic tree either the 5-branch or 3-branch approximations are used. These adjustment factors and their corresponding weights are shown in the table below (and presented in Danciu et al., 2021):


|         5 - Branch              |   |  |           3 - Branch         | |
| :-----------------: | :-----------: | --- | :-----------------: | :------: |
| Adjustment Factor   |    Weight   | --- | Adjustment Factor | Weight |
|    -2.856970      |  0.011257   |    |   -1.732051       |  0.167 |
|    -1.355630      |  0.222076   |    |         0.0       |  0.666 |
|          0.0      |  0.533334   |    |    1.732051       |  0.167 |
|     1.355630      |  0.222076   |    |          -        |    -   |
|     2.856970      |  0.011257   |    |          -        |    -   |



### GMMs

Separate GMMs are defined according to the tectonic region type:

#### 1)`ESHM20Shallow`: The main GMM for non-cratonic shallow seismicity with Vs30-based site model

This GMM is initially presented in Kotha et al. (2020) and adapted for implementation in the scaled backbone GMM logic tree for ESHM20 by Weatherill et al. (2020). The implementation contains the updates explained in Kotha et al (2022) and Weatherill et al. (2024).

Takes as optional epistemic adjustments:

`sigma_mu_epsilon`: The number of standard deviations by which to multiple the stress parameter adjustment term (default = 0.0)

`c3_epsilon`: The number of standard deviations by which to multiply the residual attenuation term (default = 0.0)

`ergodic`: Uses the ergodic within-event standard deviation (`True`) or the non-ergodic standard deviation (`False`) (default = `True`)


Requires as earthquake scenario parameter inputs:

`mag`: The earthquake magnitude

`rjb`: The Joyner-Boore distance in km

`hypo_depth`: The hypocentral depth (in km)

`vs30`: The average shearwave velocity in the upper 30 m of the crust (m/s)

`vs30measured`: A boolean variable indicating that the `vs30` refers to a _measured_ quantity (`True`) or an _inferred_ quantity (`False`)

`region`: The residual attenuation cluster region as described in Weatherill et al. (2020). This takes an integer value of 1, 2, ..., 5 for the respective attenuation region, or 0 for the _default_ region.

The GMM is imported via:

```python
from gmms.shallow_seismicity import ESHM20Shallow
```

#### 2) `ESHM20Shallow SlopeGeology`: The main GMM for non-cratonic shallow seismicity with Slope and Geology-based site model (Weatherill et al., 2023a)

This GMM is a variant `ESHM20Shallow` with the site model replaced by that calibrated for use with the ESRM20 and requires slope and geology. 

Takes the same optional epistemic adjustments as `ESHM20Shallow`

Requires as inputs:

`mag`: The earthquake magnitude

`rjb`: The Joyner-Boore distance in km

`hypo_depth`: The hypocentral depth (in km)

`slope`: The 30 arc-second slope at the site (in m/m)

`geology`: The geological unit for the GMM according to the classification presented in Weatherill et al. (2023a) - takes one of `PRECAMBRIAN`, `PALEOZOIC`, `JURASSIC-TRIASSIC`, `CRETACEOUS`, `CENOZOIC`, `PLEISTOCENE`, `HOLOCENE` or `UNKNOWN` for each site.

The GMM is imported via:

```python
from gmms.shallow_seismicity import ESHM20ShallowSlopeGeology
```

<b>NOTE: For both `ESHM20Shallow` and `ESHM20ShallowSlopeGeology` the site-specific paramters for Europe (`Vs30`, `region`, `slope`, `geology` and `xvf`) can be extracted for any site(s) in Europe using the _Exposure2Site_ Tools ([https://gitlab.seismo.ethz.ch/efehr/esrm20_sitemodel](https://gitlab.seismo.ethz.ch/efehr/esrm20_sitemodel))  prepared for the ESRM20. See Dabbeek et al. (2021) for more information.</b>

#### 3) `ESHM20Craton`: The GMM for application to the Stable Cratonic Region of Northeast Europe (Weatherill & Cotton, 2020)

The model is intended for application in stable continental regions and unlike the other models does not explicitly allow for epistemic uncertainty adjustment to the residual/anelastic attenuation term. Instead it's epistemic uncertainty is defined for the source/statistical uncertianty (`sigma_mu`) and the site term epistemic uncertainty.

Takes as optional epistemic adjustments:

`sigma_mu_epsilon`: The number of standard deviations by which to multiple the stress parameter adjustment term (default = 0.0)

`site_epsilon`: The number of standard deviations by which to multiply the epistemic uncertainty in the site amplification term (default = 0.0)

`ergodic`: Uses the ergodic within-event standard deviation (`True`) or the non-ergodic standard deviation (`False`) (default = `True`)


Requires as inputs:

`mag`: The earthquake magnitude

`rrup`: The shortest distance from the site to the rupture in km

`vs30`: The average shearwave velocity in the upper 30 m of the crust (m/s)

The GMM is imported via:

```python
from gmms.craton import ESHM20Craton
```

#### 4) `ESHM20SInter`/`ESHM20SSlab`: The GMM for application to subduction and deep seismicity sources (Weatherill et al., 2024)

The subduction GMMs are divided into subduction interface (`ESHM20SInter`) and subduction in-slab (`ESHM20SInter`) variants and are intended for application in the Hellenic, Cypriot, Calabrian and Gibraltar subduction zones. In addition, the in-slab variant is applied to the Vrancea deep seismogenic zones. The GMM is based around the "BC Hydro" GMM of Abrahamson et al. (2016) but with anelastic attenuation calibrated according to data from Europe. A further adaption

Both interface and in-slab GMMs take as optional epistemic adjustments:

`sigma_mu_epsilon`: The number of standard deviations by which to multiple the stress parameter adjustment term (default = 0.0)

`theta6_adjustment`: The amount to modify the `theta_6` (anelastic attenuation) term of the calibrated GMM in order to capture region to region epistemic uncertainty in anelastic attenuation. Should take either -0.0015, 0.0 or `0.0015` (default = 0.0)

`ergodic`: Uses the ergodic within-event standard deviation (`True`) or the non-ergodic standard deviation (`False`) (default = `True`)

`a`: When applying the smoothed Forearc/Backarc S-Function transition model this term defines the initiation of the taper in terms of distance to the volcanic front (where negative terms are in the forearc and positive terms are in the backarc. 

`b`: When applying the smoothed Forearc/Backarc S-Function transition model this term defines the termination of the taper in terms of distance to the volcanic front. 

**For the ESHM20 application a = -100 km and b = 100 km. If one or no values are set for `a` and `b` the original forearc/backarc step-function transition.**

`large_magnitude_scaling`: Can choose to apply the low, middle or high upper magnitude scaling adjustments of the original BC Hydro GMM by entering `low`, `mid` or `high` respectively (default = mid). These were not used in ESHM20

`ESHM20SInter` requires as scenario inputs:

`mag`: The earthquake magnitude

`rrup`: The shortest distance from the site to the rupture in km

`vs30`: The average shearwave velocity in the upper 30 m of the crust (m/s)

`xvf`: The distance (km) from the site to the volcanic front where `xvf >= 0` indicates the forearc and `xvf < 0` the backarc. 


`ESHM20SSlab` requires as scenario inputs:

`mag`: The earthquake magnitude

`hypo_depth`: Depth to the earthquake hypocentre (in km)

`rhypo`: The shortest distance from the site to the hypocentre (in km)

`vs30`: The average shearwave velocity in the upper 30 m of the crust (m/s)

`xvf`: The distance (km) from the site to the volcanic front where `xvf >= 0` indicates the forearc and `xvf < 0` the backarc. 

 
## Usage

To begin with, the GMMs can be imported as follows:

```
from gmms.shallow_seismicity import ESHM20Shallow, ESHM20ShallowSlopeGeology  # Shallow Seismicity Models
from gmms.craton import ESHM20Craton  # Craton Model
from gmms.subduction import ESHM20SInter, ESHM20SSlab  # Subduction interface and in-slab models
```

Each GMM is implemented in a Python object and is executed in a two-step process. In the first step (instantiation) the user sets up the object, passing to the GMM the various parameters needed to adjust the GMM according to the branch. For example:

```python
gmm = ESHM20Shallow(sigma_mu_epsilon=1.0, c3_epsilon=-1.0, ergodic=True)
```
In the second step the GMM (with all of its adjustments) is run for a set of `scenarios` (in terms of the relevant source, path and site parameters) and required intensity measure types (`imts`). This will return four outputs each with the dimensions [`Num. IMTs`, `Num. scenarios`], where `Num. IMTs` is the number of intensity measure types (the length of `imts`) and `Num. scenarios` the number of input scenarios.

`medians`: _The natural logarithm_ of the earthquke ground motion in terms of fraction of g (PGA and Sa(T)) or cm/s (for PGV)

`sigma`: The total standard deviation of the GMM (in terms of natural log units)

`tau`: The between-event standard deviation of the GMM

`phi`: The within-event standard deviation of the GMM

For example:

```python
medians, sigma, tau, phi = gmm(scenarios, imts)
```

#### Specifying `imts`

`imts` are a Python List or numpy array of required intensity measure types. For Peak Ground Acceleration (PGA) and Peak Ground Velocity (PGV) these should be specified with _lower case strings_ `pga` and `pgv` respectively. For spectral accelerations, floating point values should be input for each period T, which will be interepreted as the spectral acceleration at period T. For example, to return outputs for PGA and for Sa at T = 0.2 s, 0.5 s and 1.0 s the `imts` should be specified as:

```python
imts = ["pga", 0.2, 0.5, 1.0]
```

#### Specifying `scenarios`

The scenarios for which the ground motions are to be calculated can be input in one of several Python data types: i) Dictionary, ii) Pandas DataFrame, or iii) NumPy recarray. In each of these cases the corresponding attributes must always be vector (even if considering only one scenario) and must be accessible via the corresponding attibute key (i.e. `scenarios["mag"]`, `scenarios["rjb"]`, `scenarios["vs30"]` etc.)

##### 1) Python Dictionary

This is arguably the simplest form of input as it just requires that each vector is specified as an individual numpy array and then grouped into a dictionary. For example:

```python
import numpy as np
mags = np.array([5.0, 5.0, 6.0, 6.0, 7.0, 7.0])
dists = np.array([10.0, 100.0, 10.0, 100.0, 10.0, 100.0])
n = len(mags)

scenarios = {
    "mag": mags,
    "rjb": dists,
    "hypo_depth": np.full_like(dists, 8.0),  # All scenarios assume a hypocenter depth of 8 km
    "vs30": np.full_like(dists, 800.0),  # All sites at 800 m/s
    "vs30measured": np.zeros(n, dtype=bool),  # All sites have an inferred Vs30
    "region": np.zeros(n, dtype=int),  # All sites are in the default region (region = 0)
    "slope": np.full_like(dists, 0.08),  # All sites have a slope of 0.08 m/m
    "geology": np.array(["CENOZOIC"] * n),  # All sites are on Cenozoic geology
    }
```

##### 2) Pandas DataFrame

This is a relatively simple variant that is particularly useful if planning to read in the scenario
information from an external file, such as a csv. The above example for the Python Dictionary is implemented equivalently as a Pandas DataFrame:

```python
import numpy as np
import pandas as pd

mags = np.array([5.0, 5.0, 6.0, 6.0, 7.0, 7.0])
dists = np.array([10.0, 100.0, 10.0, 100.0, 10.0, 100.0])
n = len(mags)

scenarios = pd.DataFrame({
    "mag": mags,
    "rjb": dists,
    "hypo_depth": np.full_like(dists, 8.0),  # All scenarios assume a hypocenter depth of 8 km
    "vs30": np.full_like(dists, 800.0),  # All sites at 800 m/s
    "vs30measured": np.zeros(n, dtype=bool),  # All sites have an inferred Vs30
    "region": np.zeros(n, dtype=int),  # All sites are in the default region (region = 0)
    "slope": np.full_like(dists, 0.08),  # All sites have a slope of 0.08 m/m
    "geology": np.array(["CENOZOIC"] * n),  # All sites are on Cenozoic geology
    })
```

##### 3) Numpy recarray

This is a slightly more challenging data type to setup but it takes the shape of a numpy array with attribute-specific datatypes. This can be useful if implementing these modules in a more intensive application such as a PSHA calculation or when using Just-in-Time compilers (e.g. Numba). The required datatypes must be observed rigidly. These can be imported via:

```python
from gmms.base import eshm20_dtypes
```

The recarray is setup using:

```python
import numpy as np

mags = np.array([5.0, 5.0, 6.0, 6.0, 7.0, 7.0])
dists = np.array([10.0, 100.0, 10.0, 100.0, 10.0, 100.0])
n = len(mags)

scenario_attributes: ["mag", "rjb", "hypo_depth", "vs30",
                      "vs30measured", "region", "slope", "geology"]
scenarios_array = np.zeros(n, dtype=np.dtype([(key, eshm20_dtypes[key])
                                              for key in scenario_attributes]))

scenarios_array["mag"] = mags.copy()
scenarios_array["rjb"] = dists.copy()
scenarios_array["hypo_depth"] = np.full_like(dists, hypo_depth)
scenarios_array["vs30"] = np.full_like(dists, vs30)
scenarios_array["vs30measured"] = np.zeros(n, dtype=bool)
scenarios_array["region"] = np.zeros(n, dtype=int)
scenarios_array["slope"] = np.full_like(dists, 0.08)  # All sites have a slope of 0.08 m/m
scenarios_array["geology"] = np.array(["CENOZOIC"] * n)  # All sites are on Cenozoic geology
```

#### A more extensive set of examples of the GMM usage can be found in the notebooks included with this software


## References

[Abrahamson et al. 2016](https://journals.sagepub.com/doi/10.1193/051712EQS188MR): Abrahamson N, Gregor N, Addo K (2016) _"BC Hydro Ground Motion Prediction Equations for Subduction Earthquakes", Earthquake Spectra, 32(1): 23-44,  [https://doi:10.1193/051712EQS188MR](https://doi:10.1193/051712EQS188MR)

[Crowley et al., 2021](https://doi.org/10.7414/EUC-EFEHR-TR002-ESRM20): Crowley H, Dabbeek J, Despotaki V, Rodrigues D, Martins L, Silva V, Romão X, Pereira N, Weatherill G, Danciu L (2021) _"European Seismic Risk Model (ESRM20)"_, EFEHR Technical Report 002 v1.0.1. [https://doi.org/10.7414/EUC-EFEHR-TR002-ESRM20](https://doi.org/10.7414/EUC-EFEHR-TR002-ESRM20) 

[Dabbeek et al., 2021](https://doi.org/10.1007/s10518-021-01194-x): Dabbeek J, Crowley H, Silva V, Weatherill G, Paul N, Nievas CI (2021) _"Impact of exposure spatial resolution on seismic loss estimates in regional portfolios"_. Bulletin of Earthquake Engineering, 19: 5819 - 5841 [https://doi.org/10.1007/s10518-021-01194-x](https://doi.org/10.1007/s10518-021-01194-x) 

[Danciu et al. 2021](https://doi.org/10.12686/a15): Danciu L, Nandan S, Reyes C, Basili R, Weatherill G, Beauval C, Rovida A, Vilanova S, Sesetyan K, Bard P -Y, Cotton F, Wiemer S, Giardini D (2021) _"The 2020 Update of the European Seismic Hazard Model - ESHM20: Model Overview"._ EFEHR Technical Report 001 v1.0.0. [https://doi.org/10.12686/a15](https://doi.org/10.12686/a15)

[Kotha et al. 2020](https://doi.org/10.1007/s10518-020-00869-1): Kotha SR, Weatherill G, Bindi D, Cotton F (2020) _"A regionally-adaptable ground-motion model for shallow crustal earthquakes in Europe"_, Bulletin of Earthquake Engineering, 18: 4091 - 4125. [https://doi.org/10.1007/s10518-020-00869-1](https://doi.org/10.1007/s10518-020-00869-1)

[Kotha et al. 2022](https://doi.org/10.1007/s10518-021-01308-5): Kotha SR, Weatherill G, Bindi D, Cotton F (2022) _"Near-source magnitude scaling of spectral accelerations: analysis and update of Kotha et al. (2020) model"_, Bulletin of Earthquake Engineering, 20: 1343 - 1370. [https://doi.org/10.1007/s10518-021-01308-5](https://doi.org/10.1007/s10518-021-01308-5)

[Weatherill & Cotton, 2020](https://doi.org/10.1007/s10518-020-00940-x): Weatherill G, Cotton F (2020) _"A ground motion logic tree for seismic hazard analysis in the stable cratonic region of Europe: regionalisation, model selection and development of a scaled backbone approach"_, Bulletin of Earthquake Engineering, 18:6119 - 6148. [https://doi.org/10.1007/s10518-020-00940-x](https://doi.org/10.1007/s10518-020-00940-x)

[Weatherill et al. 2020](https://doi.org/10.1007/s10518-020-00899-9): Weatherill G, Kotha SR, Cotton F (2020) _"A regionally-adaptable "scaled backbone" ground motion logic tree for shallow seismicity in Europe: application to the 2020 European Seismic Hazard Model"_, Bulletin of Earthquake Engineering, 18: 5087 - 5117. [https://doi.org/10.1007/s10518-020-00899-9](https://doi.org/10.1007/s10518-020-00899-9)

[Weatherill et al. 2023](https://doi.org/10.1007/s10518-022-01526-5): Weatherill G, Crowley H, Roullé A, Tourlière B, Lemoine A, Gracianne C, Kotha SR, Cotton F(2023) _"Modelling site response at regional scale for the 2020 European Seismic Risk Model"_, Bulletin of Earthquake Engineering, 21: 665 - 714. [https://doi.org/10.1007/s10518-022-01526-5](https://doi.org/10.1007/s10518-022-01526-5)

[Weatherill et al. 2024](#W2024): Weatherill G, Kotha SR, Cotton F, Bindi D, Danciu L, Vilanova S (2024) _"Modelling ground motion for the 2020 European Seismic Hazard Model (ESHM20): Logic Tree, Overview and Implementation"_. Natural Hazards and Earth System Sciences, 24: 1795–1834 [https://doi.org/10.5194/nhess-24-1795-2024](https://doi.org/10.5194/nhess-24-1795-2024) 

## License
The project is licensed with a GNU General Public Licence v3.0
