from setuptools import setup, find_packages

test_requirements = ["pylint", "pytest"]

setup(
    name="ESHM20 GMMs",
    version="0.1.0dev",
    description="Minimal implementation of the GMMs for the 2020 European Seismic Hazard Model (ESHM20)",
    license="GPLv3",
    install_requires=[
        "numpy",
        "scipy",
        "pandas",
        "matplotlib",
    ],
    extras_require = {
        "dev": [
            "pylint",
            "flake8",
            "pytest",
            "pytest-cov",
            ],
    },
    packages=find_packages(),
    python_requires=">=3.8",
)


