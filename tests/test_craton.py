"""
Test suite for the Weatherill & Cotton (2020) craton models
"""
import os
from gmms.craton import ESHM20Craton
from tests.utils_test import GMMTestCase

DATA_PATH = os.path.join(os.path.dirname(__file__), "data")


class ESHM20CratonTestCase(GMMTestCase):
    """Tests for the Weatherill & Cotton (2020) GMM for application to the ESHM20 Craton region
    """
    TEST_TABLE_DIR = os.path.join(DATA_PATH, "eshm20_craton")

    def test_unadjusted_median(self):
        # Tests the basic form of the median ground motions of the model without adjustments
        self.execute_model_test(ESHM20Craton, "ESHM20_CRATON_MEAN_MEDIAN_MODEL.csv", {})

    def test_unadjusted_total_sigma_ergodic(self):
        # Tests the basic form of the ergodic total standard deviations of the model without
        # adjustments
        self.execute_model_test(ESHM20Craton, "ESHM20_CRATON_TOTAL_STDDEV_ERGODIC.csv", {})

    def test_median_plus_1_epsilon(self):
        # Tests the basic form of the median model with a simple adjustment to the median
        self.execute_model_test(ESHM20Craton,
                                "ESHM20_CRATON_MEAN_PLUS1SIGMA_MODEL.csv",
                                {"epsilon": 1.0})

    def test_unadjusted_total_sigma_nonergodic(self):
        # Tests the basic form of the non-ergodic total standard deviations of the model
        self.execute_model_test(ESHM20Craton,
                                "ESHM20_CRATON_TOTAL_STDDEV_NONERGODIC.csv",
                                {"ergodic": False})
