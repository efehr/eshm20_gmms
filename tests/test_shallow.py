"""
Test suite for the GMMs for shallow seismicity (Kotha et al., 2020; 2022; Weatherill et al.,
2020; 2023a; 2023b)
"""
import os
from gmms.shallow_seismicity import ESHM20Shallow, ESHM20ShallowSlopeGeology
from tests.utils_test import GMMTestCase

DATA_PATH = os.path.join(os.path.dirname(__file__), "data")


class ESHM20ShallowTestCase(GMMTestCase):
    """Tests for the shallow seismicity GMM for application to the ESHM20
    """
    TEST_TABLE_DIR = os.path.join(DATA_PATH, "eshm20_shallow")

    def test_unadjusted_median(self):
        # Tests the basic form of the model for ESHM20 without any epistemic adjustments
        self.execute_model_test(ESHM20Shallow, "KOTHA_2020_ESHM_CENTRAL_MEAN.csv", {})

    def test_unadjusted_stddevs(self):
        # Tests the standard deviations of the model without any adjustments
        # Total stddev.
        self.execute_model_test(ESHM20Shallow, "KOTHA_2020_ESHM_STDDEV_TOTAL.csv", {})
        # Inter-event stddev.
        self.execute_model_test(ESHM20Shallow, "KOTHA_2020_ESHM_STDDEV_INTER_EVENT.csv", {})
        # Intra-event stddev
        self.execute_model_test(ESHM20Shallow, "KOTHA_2020_ESHM_STDDEV_INTRA_EVENT.csv", {})

    def test_high_sigma_mu_case(self):
        # Tests the form with a high sigma mu adjustment
        self.execute_model_test(ESHM20Shallow, "KOTHA_2020_ESHM_HIGH_SIGMA_MU_MEAN.csv",
                                {"sigma_mu_epsilon": 1.0})

    def test_fast_attenuation_case(self):
        # Tests the form of the model with a fast attenuation adjustment
        self.execute_model_test(ESHM20Shallow, "KOTHA_2020_ESHM_FAST_ATTEN_MEAN.csv",
                                {"c3_epsilon": -1.0})

    def test_nonergodic_stddev_case(self):
        # Tests the case with the non-ergodic standard deviation model
        # Intra-event
        self.execute_model_test(ESHM20Shallow,
                                "KOTHA_2020_ESHM_STDDEV_NONERGODIC_INTRA_EVENT.csv",
                                {"ergodic": False})

        # Total stddev
        self.execute_model_test(ESHM20Shallow,
                                "KOTHA_2020_ESHM_STDDEV_NONERGODIC_TOTAL.csv",
                                {"ergodic": False})

    def test_slope_geology_case_median(self):
        # Tests the median ground motion for the slope and geology case
        self.execute_model_test(ESHM20ShallowSlopeGeology,
                                "KOTHA_2020_SLOPE_GEOLOGY_MEAN.csv", {})

    def test_slope_geology_case_stddevs(self):
        # Tests the median ground motion for the slope and geology case
        # Intra-event
        self.execute_model_test(ESHM20ShallowSlopeGeology,
                                "KOTHA_2020_SLOPE_GEOLOGY_INTRA_EVENT_STDDEV.csv", {})
        # Inter-event
        self.execute_model_test(ESHM20ShallowSlopeGeology,
                                "KOTHA_2020_SLOPE_GEOLOGY_INTER_EVENT_STDDEV.csv", {})
        # Total
        self.execute_model_test(ESHM20ShallowSlopeGeology,
                                "KOTHA_2020_SLOPE_GEOLOGY_TOTAL_STDDEV.csv", {})
