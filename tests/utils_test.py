"""Utility functions for running the GMM tests
"""
import os
import unittest
from typing import List, Tuple, Dict
import numpy as np
import pandas as pd
from gmms.base import GMM


def oq_test_table_to_dataframe(test_file) -> Tuple[pd.DataFrame, List, pd.DataFrame, str]:
    """Reads in a test table (OpenQuake formatted) and extracts the metadata (source, path
    and site parameters), the intensity measure types and the ground motion values)

    Args
        test_file: Path to the test table csv file

    Returns:
        metadata: Scenario metadata organised as pandas DataFrame
        imts: List of intensity measure types
        imls: Ground motion values as pandas DataFrame
        result_type: The type of result (e.g. "MEAN", "TOTAL_STDDEV", "INTER_EVENT_STDDEV",
                     "INTRA_EVENT_STDDEV")
    """
    test_data = pd.read_csv(test_file, sep=",")
    metadata = {}
    imls = []
    result_type = test_data["result_type"].iloc[0]
    if not np.all(test_data["result_type"] == result_type):
        raise ValueError("Test table file %s has mixed result types - incorrect format"
                         % test_file)
    for hdr in test_data.columns:
        if hdr.startswith("rup") or hdr.startswith("dist") or hdr.startswith("site"):
            # Is a metadata column
            metadata[hdr] = "_".join(hdr.split("_")[1:])
        elif hdr in ("result_type", "damping"):
            continue
        else:
            # Is an IMT column
            imls.append(hdr)
    imts = [imt if imt in ("pga", "pgv") else float(imt) for imt in imls]
    imls = test_data[imls]
    metadata = test_data[list(metadata)].rename(columns=metadata, inplace=False)
    return metadata, imts, imls, result_type


RESULT_POSITIONS = {
    "MEAN": 0,
    "TOTAL_STDDEV": 1,
    "INTER_EVENT_STDDEV": 2,
    "INTRA_EVENT_STDDEV": 3
}


class GMMTestCase(unittest.TestCase):
    """Common class to execute GMM tests using OpenQuake test tables
    """
    TEST_TABLE_DIR = None

    def execute_model_test(self, TestGMM: GMM, test_file: str, gmm_kwargs: Dict):
        """Executes the test for the given test file and the adjustment factors
        """
        metadata, imts, imls, result_type = oq_test_table_to_dataframe(
            os.path.join(self.TEST_TABLE_DIR, test_file)
            )
        pos = RESULT_POSITIONS[result_type]
        if result_type == "MEAN":
            expected = np.log(imls.to_numpy())
        else:
            expected = imls.to_numpy()
        # Instantiate and run the GMM to retrieve the median motion
        gmm = TestGMM(**gmm_kwargs)
        medians = gmm(metadata, imts)[pos]
        np.testing.assert_array_almost_equal(expected, medians)
        return
