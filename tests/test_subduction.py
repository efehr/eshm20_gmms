"""
Test suite for the Weatherill et al (2023) subduction models
"""
import os
from gmms.subduction import ESHM20SInter, ESHM20SSlab
from tests.utils_test import GMMTestCase


DATA_PATH = os.path.join(os.path.dirname(__file__), "data")


class ESHM20SInterTestCase(GMMTestCase):
    """Test case for the ESHM20 subduction interface model
    """
    TEST_TABLE_DIR = os.path.join(DATA_PATH, "eshm20_subduction_deep")

    def test_unadjusted_case(self):
        # Tests the basic form of the median ground motions of the model without adjustments
        self.execute_model_test(ESHM20SInter, "BCHydroESHM20_SINTER_CENTRAL_MEAN.csv", {})

    def test_upper_mag_scaling_case(self):
        # Tests the basic form of the median ground motions of the model for the "high"
        # large magnitude scaling branch
        self.execute_model_test(ESHM20SInter, "BCHydroESHM20_SINTER_UPPER_MEAN.csv",
                                {"large_magnitude_scaling": "high"})

    def test_lower_mag_scaling_case(self):
        # Tests the basic form of the median ground motions of the model for the "high"
        # large magnitude scaling branch
        self.execute_model_test(ESHM20SInter, "BCHydroESHM20_SINTER_LOWER_MEAN.csv",
                                {"large_magnitude_scaling": "low"})

    def test_faster_attenuation_case(self):
        # Tests the basic form of the median ground motions of the model for the fast
        # attenuation case
        self.execute_model_test(ESHM20SInter, "BCHydroESHM20_SINTER_FAST_MEAN.csv",
                                {"theta6_adjustment": -0.0015})

    def test_high_sigma_mu_case(self):
        # Tests the basic form of the median ground motions of the model for the high
        # sigma mu epsilon case
        self.execute_model_test(ESHM20SInter, "BCHydroESHM20_SINTER_HIGH_SIGMA_MU_MEAN.csv",
                                {"sigma_mu_epsilon": 1.0})

    def test_sfunction_faba_taper_case(self):
        # Tests the form of the median ground motions of the model with the S-Function forearc/
        # backarc taper
        self.execute_model_test(ESHM20SInter,
                                "BCHydroESHM20_SINTER_SFUNC_FABA_TAPER_MEAN.csv",
                                {"a": -100.0, "b": 100.0})

    def test_total_stddev(self):
        # Tests the total standard devation
        self.execute_model_test(ESHM20SInter, "BCHYDRO_SINTER_CENTRAL_STDDEV_TOTAL.csv", {})

    def test_inter_event_stddev(self):
        # Tests the inter-event standard deviation
        self.execute_model_test(ESHM20SInter, "BCHYDRO_SINTER_CENTRAL_STDDEV_INTER.csv", {})

    def test_intra_event_stddev(self):
        # Tests the inter-event standard deviation
        self.execute_model_test(ESHM20SInter, "BCHYDRO_SINTER_CENTRAL_STDDEV_INTRA.csv", {})

    def test_nonergodic_total_stddev(self):
        # Tests the total standard devation
        self.execute_model_test(ESHM20SInter,
                                "BCHYDRO_SINTER_CENTRAL_STDDEV_NONERGODIC_TOTAL.csv",
                                {"ergodic": False})

    def test_nonergodic_intra_event_stddev(self):
        # Tests the inter-event standard deviation
        self.execute_model_test(ESHM20SInter,
                                "BCHYDRO_SINTER_CENTRAL_STDDEV_NONERGODIC_INTRA.csv",
                                {"ergodic": False})


class ESHM20SSlabTestCase(GMMTestCase):
    """Test case for the ESHM20 subduction in-slab model
    """
    TEST_TABLE_DIR = os.path.join(DATA_PATH, "eshm20_subduction_deep")

    def test_unadjusted_case(self):
        # Tests the basic form of the median ground motions of the model without adjustments
        self.execute_model_test(ESHM20SSlab, "BCHydroESHM20_SSLAB_CENTRAL_MEAN.csv", {})

    def test_upper_mag_scaling_case(self):
        # Tests the basic form of the median ground motions of the model for the "high"
        # large magnitude scaling branch
        self.execute_model_test(ESHM20SSlab, "BCHydroESHM20_SSLAB_UPPER_MEAN.csv",
                                {"large_magnitude_scaling": "high"})

    def test_lower_mag_scaling_case(self):
        # Tests the basic form of the median ground motions of the model for the "high"
        # large magnitude scaling branch
        self.execute_model_test(ESHM20SSlab, "BCHydroESHM20_SSLAB_LOWER_MEAN.csv",
                                {"large_magnitude_scaling": "low"})

    def test_faster_attenuation_case(self):
        # Tests the basic form of the median ground motions of the model for the fast
        # attenuation case
        self.execute_model_test(ESHM20SSlab, "BCHydroESHM20_SSLAB_FAST_MEAN.csv",
                                {"theta6_adjustment": -0.0015})

    def test_high_sigma_mu_case(self):
        # Tests the basic form of the median ground motions of the model for the high
        # sigma mu epsilon case
        self.execute_model_test(ESHM20SSlab, "BCHydroESHM20_SSLAB_HIGH_SIGMA_MU_MEAN.csv",
                                {"sigma_mu_epsilon": 1.0})

    def test_sfunction_faba_taper_case(self):
        # Tests the form of the median ground motions of the model with the S-Function forearc/
        # backarc taper
        self.execute_model_test(ESHM20SSlab, "BCHydroESHM20_SSLAB_SFUNC_FABA_TAPER_MEAN.csv",
                                 {"a": -100.0, "b": 100.0})

    def test_total_stddev(self):
        # Tests the total standard devation
        self.execute_model_test(ESHM20SSlab, "BCHYDRO_SSLAB_CENTRAL_STDDEV_TOTAL.csv", {})

    def test_inter_event_stddev(self):
        # Tests the inter-event standard deviation
        self.execute_model_test(ESHM20SSlab, "BCHYDRO_SSLAB_CENTRAL_STDDEV_INTER.csv", {})

    def test_intra_event_stddev(self):
        # Tests the inter-event standard deviation
        self.execute_model_test(ESHM20SSlab, "BCHYDRO_SSLAB_CENTRAL_STDDEV_INTRA.csv", {})
